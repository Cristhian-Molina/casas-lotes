import {Ajax} from './helpers/ajax.js';

const $form_nuevo_comprador = document.querySelector('#form_nuevo_comprador');


//INSTANCIA DE LA CLASE Ajax
const ajax = new Ajax();

class Comprador{

		validarDatosComprador(){
				let nombres               = $form_nuevo_comprador.nombres;
				let apellidos             = $form_nuevo_comprador.apellidos;
				let tipo_documento        = $form_nuevo_comprador.tipo_documento.selectedIndex;
				let numero_identificacion = $form_nuevo_comprador.numero_identificacion;
				let dia_pago              = $form_nuevo_comprador.dia_pago.selectedIndex;

				if(nombres.value == ''){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer los nombres del comprador esta vacio!'
						});
						return false;
				}else if(apellidos.value == ''){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer los apellidos del comprador esta vacio!'
						});
						return false;
				}else if(tipo_documento == null || tipo_documento == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste el tipo de documento del comprador!'
						});
						return false;
				}else if(numero_identificacion.value == ''){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer el numero de identificacion del comprador esta vacio!'
						});
						return false;
				}else if(dia_pago == null || dia_pago == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer el día de pago esta vacio'
						});
						return false;
				}

				return true;
		}

		async guardarNuevoComprador(form){
				try{
						let peticion = await ajax.fetchData('AjaxRequest/agregar_nuevo_comprador/', form, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}




}


const comprador = new Comprador();

document.addEventListener('DOMContentLoaded', ()=>{

const $dia_pago = document.querySelector('#dia_pago');

for (let i = 0; i < 31; i++) {

	$dia_pago.options[i] = new Option(i+1, (i+1));

}


});


//LISTENER
$form_nuevo_comprador.addEventListener('submit', e =>{
		e.preventDefault();

		if (comprador.validarDatosComprador()) {
					let form = new FormData(e.target);

					comprador.guardarNuevoComprador(form)
					.then(comprador => {
							if (comprador.estado_respuesta == 0) {
									Swal.fire({
											type: 'error',
											title: 'Opps..!',
											text: 'Datos sin poderse guardar!'
									});
									return false;
							} else{
									Swal.fire({
											type: 'success',
											title: 'Genial..!',
											text: 'Datos guardados con exito!'
									})
									.then(() =>{
												window.location.href = `${url_javascript}comprador/compra_lote/${comprador.id_comprador}`;
										});
							}
					});
		}
});