import {Ajax} from './helpers/ajax.js';

const $cargar_estimado_contado = document.querySelector('.cargar_estimado_contado');
const $error_mostrar = document.querySelector('.error');
const $total_lotes   = document.querySelector('.total_lotes');
const $btn_generar_pdf = document.querySelector('#btn_generar_pdf');

const ajax = new Ajax();

class Contado{

	async cargarEstimadoContado(){
		try{
				let peticion = await ajax.fetchData('AjaxRequest/cargar_estimado_contado/');
				return peticion;
		} catch(e){
			console.log('ERROR =>', e);
		}
	}

	formatoNumeroMiles(n){
			return new Intl.NumberFormat('es-CO', {
				style : 'currency',
				currency : 'COP',
				maximumFractionDigits: 0,
				minimunFractionDigits : 0
			}).format(n);
	}

	quitarFormatoMoneda(moneda){

			moneda = moneda.replace("$","");
			moneda = moneda.replaceAll(".","");
			moneda = parseFloat(moneda);
			return(moneda); //returns --> 1200.51

	}

	sumandoArray(array){

		return array.reduce((a, b) => a + b, 0);
	}

	mostrarEstimadoContado(contado){

		if (contado.estado_respuesta == 1) {
				$btn_generar_pdf.removeAttribute('disabled');
				let listado = '';
				let datos = contado.data;

				//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
				let array_valor_lote = [];
				datos.forEach(estado =>{
						let tipo_lote = (estado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';

						let valor_lote = this.quitarFormatoMoneda(estado.valor);
						//AGREGANDO TODOS LOS VALORES A UN ARRAY
						array_valor_lote.push(valor_lote);
						listado +=`
						<tr>
							<td>${estado.numero_lote}</td>
							<td>${tipo_lote}</td>
							<td>${estado.nombre}</td>
							<td>${estado.valor}</td>
						</tr>`;
				});

				$cargar_estimado_contado.innerHTML = listado;

				let suma_valor_lotes = this.sumandoArray(array_valor_lote);
				$total_lotes.textContent = this.formatoNumeroMiles(suma_valor_lotes);
		} else{
				$error_mostrar.style.display = 'block';
				$btn_generar_pdf.setAttribute('disabled', 'disabled');
		}
	}

	generarPdfEstimado(estimados){

		let doc = new jspdf.jsPDF()
		doc.addImage({
				imageData : `${url_javascript}public/img/logo-puertas-del-sol.jpg`,
				x         : 160,
				y         : 10,
				w         : 25,
				h         : 29
		});

		doc.setFont("courier");
		doc.setFontSize(12);
		let fecha = new Date();
		let options = {year: 'numeric', month: 'long', day: 'numeric' };
		let array = [];
		//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
		let array_valor_lote = [];
		let array_valor_pagar = [];
		let array_valor_interes =[];
		let array_totales = [];

		doc.setFont('Courier', 'bold');
		doc.text(`Jamundi Valle,`, 10, 60);
		doc.setFont("courier", 'normal');
		doc.text(`${fecha.toLocaleDateString("es-ES", options)}`, 48, 60)
		doc.setFontSize(12);
		doc.setFont('Courier', 'bold');
		doc.text('ESTIMADO DE VENTAS POR ESTADO DE CONTADO', 100, 80, {align: 'center'});

		estimados.data.forEach(estimado =>{

				let tipo_lote = (estimado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';
				let valor_lote = this.quitarFormatoMoneda(estimado.valor);

				//AGREGANDO TODOS LOS VALORES A UN ARRAY
				array_valor_lote.push(valor_lote);

				array.push([estimado.numero_lote, tipo_lote, estimado.nombre, estimado.valor]);

		});

		let suma_valor_lotes = this.sumandoArray(array_valor_lote);

		array_totales.push([this.formatoNumeroMiles(suma_valor_lotes)]);

		let head_total = [['Total Valor Lotes']];
		let body_total = array_totales;
		doc.autoTable({
			head: head_total,
			body: body_total,
			startY: 95,
			theme : 'grid',
			styles:{
					halign: 'center'
			}
		});

		let head = [['# Lote', 'Tipo Lote', 'Cliente','Valor Lote']]
		let body = array;
		doc.autoTable({
			head: head,
			body: body ,
			startY: 120,
			styles: {
					halign: 'center'
			}
		});
		window.open(doc.output('bloburl'), '_blank');

	}


}

const contado = new Contado();

document.addEventListener('DOMContentLoaded', e=>{

	contado.cargarEstimadoContado()
	.then(estado =>{
			contado.mostrarEstimadoContado(estado);
	});
});

$btn_generar_pdf.addEventListener('click', e =>{

	contado.cargarEstimadoContado()
	.then(resp =>{
			contado.generarPdfEstimado(resp);
	});
});