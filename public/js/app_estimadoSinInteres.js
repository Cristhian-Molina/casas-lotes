import {Ajax} from './helpers/ajax.js';

const $cargar_estimado_financiado_sin_interes = document.querySelector('.cargar_estimado_financiado_sin_interes');
const $error_mostrar = document.querySelector('.error');
const $total_lotes   = document.querySelector('.total_lotes');
const $total_ingreso = document.querySelector('.total_ingreso');
const $btn_generar_pdf = document.querySelector('#btn_generar_pdf');

const ajax = new Ajax();

class SinInteres{

	async cargarEstimadoFinanciadoSinInteres(){
		try{
				let peticion = await ajax.fetchData('AjaxRequest/cargar_estimado_financiado_sin_interes/');
				return peticion;
		} catch(e){
			console.log('ERROR =>', e);
		}
	}

	formatoNumeroMiles(n){
			return new Intl.NumberFormat('es-CO', {
				style : 'currency',
				currency : 'COP',
				maximumFractionDigits: 0,
				minimunFractionDigits : 0
			}).format(n);
	}

	quitarFormatoMoneda(moneda){

			moneda = moneda.replace("$","");
			moneda = moneda.replaceAll(".","");
			moneda = parseFloat(moneda);
			return(moneda); //returns --> 1200.51

	}

	sumandoArray(array){

		return array.reduce((a, b) => a + b, 0);
	}

	mostrarEstimadoFinanciadoSinInteres(response){

		if (response.estado_respuesta == 1) {
				$btn_generar_pdf.removeAttribute('disabled');
				let listado = '';
				let datos = response.data;
				//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
				let array_valor_lote = [];
				let array_valor_pagar = [];
				datos.forEach(estado =>{
						let tipo_lote = (estado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';
						let valor_pagar = this.quitarFormatoMoneda(estado.valor_pagar);
						let valor_lote = this.quitarFormatoMoneda(estado.valor);

						//AGREGANDO TODOS LOS VALORES A UN ARRAY
						array_valor_lote.push(valor_lote);
						array_valor_pagar.push(valor_pagar);


						listado +=`
						<tr>
							<td>${estado.numero_lote}</td>
							<td>${tipo_lote}</td>
							<td>${estado.nombre}</td>
							<td>${estado.valor}</td>
							<td>${estado.numero_cuotas}</td>
							<td>${estado.valor_pagar}</td>
						</tr>`;
				});

				$cargar_estimado_financiado_sin_interes.innerHTML = listado;

				let suma_valor_lotes = this.sumandoArray(array_valor_lote);
				let suma_valor_ingreso = this.sumandoArray(array_valor_pagar);

				$total_lotes.textContent = this.formatoNumeroMiles(suma_valor_lotes);
				$total_ingreso.textContent = this.formatoNumeroMiles(suma_valor_ingreso);
		} else{
				$error_mostrar.style.display = 'block';
				$btn_generar_pdf.setAttribute('disabled', 'disabled');
		}
	}

	generarPdfEstimado(estimados){

		let doc = new jspdf.jsPDF()
		doc.addImage({
				imageData : `${url_javascript}public/img/logo-puertas-del-sol.jpg`,
				x         : 160,
				y         : 10,
				w         : 25,
				h         : 29
		});

		doc.setFont("courier");
		doc.setFontSize(12);
		let fecha = new Date();
		let options = {year: 'numeric', month: 'long', day: 'numeric' };
		let array = [];

		//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
		let array_valor_lote = [];
		let array_valor_pagar = [];
		let array_totales = [];

		doc.setFont('Courier', 'bold');
		doc.text(`Jamundi Valle,`, 10, 60);
		doc.setFont("courier", 'normal');
		doc.text(`${fecha.toLocaleDateString("es-ES", options)}`, 48, 60)
		doc.setFontSize(12);
		doc.setFont('Courier', 'bold');
		doc.text('ESTIMADO DE VENTAS POR ESTADO FINANCIADO SIN INTERES', 100, 80, {align: 'center'});

		estimados.data.forEach(estimado =>{

				let tipo_lote = (estimado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';
				let valor_pagar = this.quitarFormatoMoneda(estimado.valor_pagar);
				let valor_lote = this.quitarFormatoMoneda(estimado.valor);

				//AGREGANDO TODOS LOS VALORES A UN ARRAY
				array_valor_lote.push(valor_lote);
				array_valor_pagar.push(valor_pagar);

				array.push([estimado.numero_lote, tipo_lote, estimado.nombre, estimado.valor, estimado.numero_cuotas, estimado.valor_pagar]);

		});

		let suma_valor_lotes = this.sumandoArray(array_valor_lote);
		let suma_valor_ingreso = this.sumandoArray(array_valor_pagar);

		array_totales.push([this.formatoNumeroMiles(suma_valor_lotes), this.formatoNumeroMiles(suma_valor_ingreso)]);

		let head_total = [['Total Valor Lotes', 'Total Ingreso Constructora']];
		let body_total = array_totales;
		doc.autoTable({
			head: head_total,
			body: body_total,startY: 95,
			theme: 'grid',
			styles: {
					halign: 'center'
			}
		});

		let head = [['# Lote', 'Tipo Lote', 'Cliente','Valor Lote', 'Cuotas', 'Valor Ingreso Cons.']]
		let body = array;
		doc.autoTable({
			head: head,
			body: body,
			startY: 120,
			styles: {
				halign: 'center'
			}
		});
		window.open(doc.output('bloburl'), '_blank');
	}


}

const sinInteres = new SinInteres();

document.addEventListener('DOMContentLoaded', e =>{

		sinInteres.cargarEstimadoFinanciadoSinInteres()
				.then(estado =>{
						sinInteres.mostrarEstimadoFinanciadoSinInteres(estado);
				});
});

$btn_generar_pdf.addEventListener('click', e =>{

	sinInteres.cargarEstimadoFinanciadoSinInteres()
	.then(resp =>{
			sinInteres.generarPdfEstimado(resp);
	});
});