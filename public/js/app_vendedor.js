import {Ajax} from './helpers/ajax.js';

const $form_nuevo_vendedor      = document.querySelector('#nuevo_vendedor_form');
const $cargar_vendedoresDOM     = document.querySelector('#cargar_vendedores');
const rootEl                    = document.documentElement;
const $modal                    = document.querySelector('.modal');
const $actualizar_vendedores    = document.querySelector('#actualizar_vendedores');
const $error_mostrar_vendedores = document.querySelector('.error');
const $modal_content            = document.querySelector('.modal');

//INSTANCIA DE LA CLASE AJAX
const ajax = new Ajax();


class Vendedor{

	async sendFormVendedor(form){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/agregar_nuevo_vendedor/', form, 'POST');
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}

	async sendFormVendedorActualizar(form){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/actualizar_un_vendedor', form, 'POST');
					return peticion;
			} catch(e){
					console.log('ERROR =>', e);
			}
	}

	async cargarTodosVendedores(){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/cargar_todos_vendedores/');
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}

	async cargarUnVendedor(vendedor){
			try	{
					let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_vendedor/${vendedor}`);
					return peticion;
			} catch(e){
					console.log('ERROR =>', e);
			}
	}

	async eliminarUnVendedor(eliminarVendedor){

			try{
					let peticion = await ajax.fetchData(`AjaxRequest/eliminar_un_vendedor/${eliminarVendedor}`);
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}



	validarDatosVendedor(){

		let nombres               = $form_nuevo_vendedor.nombres;
		let apellidos             = $form_nuevo_vendedor.apellidos;
		let tipo_documento        = $form_nuevo_vendedor.tipo_documento.selectedIndex;
		let numero_identificacion = $form_nuevo_vendedor.numero_identificacion;
		let celular               = $form_nuevo_vendedor.celular;

		if (nombres.value == '') {
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer los nombres del vendedor esta vacio!'
				});
				return false;
		} else if(apellidos.value == ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer los apellidos del vendedor esta vacio!'
				});
				return false;
		} else if(tipo_documento == null || tipo_documento == 0){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer no seleccionaste el tipo de documento del vendedor!'
				});
				return false;
		} else if(numero_identificacion.value == ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer el numero de identificacion del vendedor esta vacio!'
				});
				return false;
		} else if(celular.value == ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer el numero de celular del vendedor esta vacio!'
				});
				return false;
		}
		return true;
	}

	mostrarVendedoresDOM(vendedores){
			if(vendedores.estado_respuesta == 0){
					$error_mostrar_vendedores.style.display = 'block';
			} else{
					let listado = '';
					let datos = vendedores.data;
					datos.forEach(vendedor =>{
							listado +=
							`<tr>
								<td width="5%">${vendedor.id}</td>
								<td>${vendedor.nombres}</td>
								<td class="level-right">
									<a class="button is-small is-info mr-1 modal-update" data-id="${vendedor.id}">Actualizar</a>
									<a class="button is-small is-danger modal-delete" data-id="${vendedor.id}">Eliminar</a>
								</td>
							</tr>`
					});
					$cargar_vendedoresDOM.innerHTML = listado;
			}
	}

	mostrarUnVendedorDOM(target){
			this.cargarUnVendedor(target)
			.then(data =>{
					if (data.estado_respuesta == 1) {
							this.modalEditarVendedor();

							//TRAEMOS TODOS LOS CAMPOS DEL MODAL PARA ACTUALIZAR
							let nombres      = $actualizar_vendedores.nombres_update,
							apellidos        = $actualizar_vendedores.apellidos_update,
							tipo_documento   = $actualizar_vendedores.tipo_documento_update,
							numero_documento = $actualizar_vendedores.numero_identificacion_update,
							celular          = $actualizar_vendedores.celular_update,
							id_vendedor      = $actualizar_vendedores.id_vendedor_update;

							//CARGAMOS LOS VALORES DE LA BASE DE DATOS EN EL MODAL PARA ACTUALIZAR
							nombres.value          = data.data['nombres'];
							apellidos.value        = data.data['apellidos'];
							tipo_documento.value   = data.data['id_identificacion'];
							numero_documento.value = data.data['numero_identificacion'];
							celular.value          = data.data['celular'];
							id_vendedor.value      = data.data['id'];
					}
				});
	}


	openModal(){

			$modal.classList.add('is-active');
			rootEl.classList.add('is-clipped');
	}

	closeModals(){
		rootEl.classList.remove('is-clipped');
		$modal.classList.remove('is-active');
	}


	modalEditarVendedor(){
			this.openModal();
	}

}//END CLASS VENDEDOR

//INSTANCIA DE LA CLASE VENDEDOR
const vendedor = new Vendedor();

document.addEventListener('DOMContentLoaded', () =>{
		//LLAMADA AL CONTROLADOR PARA TRAER LA INFORMACION
		vendedor.cargarTodosVendedores()
		.then(vendedores =>{
				//LLAMADA AL METODO PARA MOSTRAR EN EL DOM LA INFORMACION
				vendedor.mostrarVendedoresDOM(vendedores);

				$cargar_vendedoresDOM.addEventListener('click', (e) =>{
				let id_vendedor = e.target.dataset.id;

					if (e.target.classList.contains('modal-update')) {
							vendedor.mostrarUnVendedorDOM(id_vendedor);
					}
					//ELIMINAR UN VENDEDOR
					if (e.target.classList.contains('modal-delete')) {
							Swal.fire({
									title: 'Estas seguro en Eliminar Este vendedor?',
									text: "No se podra revertir esto!",
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: 'Si, eliminalo!'
							})
							.then(result =>{
									if (result.value) {
											vendedor.eliminarUnVendedor(e.target.dataset.id)
											.then(dato => {
													if (dato.estado_respuesta == 1) {
																Swal.fire({
																title:'Eliminado!',
																text:'El vendedor fue eliminado',
																type:'success'
															})
															.then(()=>{
																	location.reload();
															})
													}
											});

									}
							})
					}
				});
		});

		$modal_content.addEventListener('click', e =>{
				if (e.target.classList.contains('cancel-update')) {
						vendedor.closeModals();
				}
		});

}); //FIN LISTENER DOMCONTENTLOADED





$form_nuevo_vendedor.addEventListener('submit', e =>{
		e.preventDefault();
		vendedor.validarDatosVendedor();
		if (vendedor.validarDatosVendedor()) {
				let form = new FormData($form_nuevo_vendedor);

				vendedor.sendFormVendedor(form)
				.then(data =>{
						if (data.estado_respuesta == 0) {
								Swal.fire({
									type: 'error',
									title: 'Opps..!',
									text: 'Datos sin poderse guardar!'
								});
								return false;
						} else if(data.estado_respuesta == 1){
								Swal.fire({
									type: 'success',
									title: 'Genial..!',
									text: 'Datos Guardados con exito!'
								});
						}
				})
				.then(() =>{
						vendedor.cargarTodosVendedores()
						.then(vendedores =>{

								$error_mostrar_vendedores.style.display = 'none';
								vendedor.mostrarVendedoresDOM(vendedores);
						})

				});
		}
});


$actualizar_vendedores.addEventListener('submit', e =>{
		e.preventDefault();
		let form = new FormData(e.target);

		vendedor.sendFormVendedorActualizar(form)
		.then(data =>{
				if(data.estado_respuesta == 0){
						Swal.fire({
							type: 'error',
							title: 'Opps..!',
							text: 'Datos sin poderse actualizar!'
						});
						return false;
				}else{
						Swal.fire({
							type: 'success',
							title: 'Genial..!',
							text: 'Datos actualizados con exito!'
						})
						.then(() =>{
								vendedor.closeModals();
								vendedor.cargarTodosVendedores()
								.then(vendedores =>{
										vendedor.mostrarVendedoresDOM(vendedores);
								})
						});
				}
		})

});

document.addEventListener('keydown', event => {
		let e = event || window.event;

		if (e.keyCode === 27) {
				vendedor.closeModals();
		}
});
