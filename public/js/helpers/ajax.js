//CLASE DE LLAMADAS ASYNCRONAS CON FETCH API
export class Ajax {

	async fetchData(controlador, parametros, metodo = 'GET') {
			//OPCIONES A LA SOLICITUD DE LA BUSQUEDA
			let opciones = {
							method : metodo,
							headers: {
									'Accept': 'text/plain'
								}
					};

			//SI EXISTEN PARAMETROS Y EL METODO ES DIFERENTE DE GET AGREGUE AL CUERPO DE LA SOLICITUD LOS PARAMETROS
			if (parametros) {
					if (metodo !== 'GET') {
							//AGRERAR AL CUERPO DEL OBJETO DE OPCIONES LOS PARAMETROS
							opciones.body = parametros;


							//RESPUESTA DEL SERVIDOR
							let respuesta = await fetch(url_javascript + controlador, opciones);

							//MUESTRA UN ERROR SI EL CODIGO DE ESTADO NO ES 200
							if (respuesta.status !== 200) {
									return {
										status: 'error',
										'mensaje' : 'El servidor respondió con un estado inesperado.'
									}
							}
							//DEVOLVEMOS LA RESPUESTA
							let data = await respuesta.json();
							//let data = await respuesta.JSON.parse();
							return data;

					}
			} else{
					//FETCH DEVUELVE UNA PROMESA, POR LO QUE AGREGAMOS LA PALABRA CLAVE AWAIT PARA ESPERAR HASTA QUE SE CUMPLA LA PROMESA
					let respuesta = await fetch(url_javascript + controlador, opciones);

					//MUESTRA UN ERROR SI EL CODIGO DE ESTADO NO ES 200
					if (respuesta.status !== 200) {
							return{
								status: 'error',
								'mensaje' : 'El servidor respondió con un estado inesperado.'
							}
					}
					//DEVUELVE LA RESPUESTA DE LA INFORMACION
					let datos = await respuesta.json();
					return datos;
			}

	}// End metodo getData
}// End class Ajax