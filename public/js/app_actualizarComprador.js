import {Ajax} from './helpers/ajax.js';

const $select_tipo_busqueda       = document.querySelector('#select_tipo_busqueda');
const $form_buscar_cedula         = document.querySelector('#form_buscar_cedula');
const $form_buscar_lote           = document.querySelector('#form_buscar_lote');
const $buscar_nombre_input        = document.querySelector('#buscar_nombre_input');
const $form_actualizar_comprador  = document.querySelector('#form_actualizar_comprador');
const $mostrar_resultado_busqueda = document.querySelector('#mostrar_resultado_busqueda');
const rootEl                      = document.documentElement;
const $modal                      = document.querySelector('.modal');


const ajax = new Ajax();

class ActualizarComprador{

		async cargarComprador(form){
				try{
						let peticion = await ajax.fetchData('AjaxRequest/cargar_un_comprador/', form, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async cargarCompradorID(id_comprador){
				try	{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_comprador_id/${id_comprador}`);
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}

		async cargarCompradorPorLote(form){
				try	{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_comprador_lote/`, form, 'POST');
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}

		async actualizarUnComprador(form){
				try{
						let peticion = await ajax.fetchData('AjaxRequest/actualizar_un_comprador', form, 'POST');
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}

		async cargarCompradorPorNombre(form){
				try	{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_comprador_nombre/`, form, 'POST');
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}

		mostrarBloquesBusqueda(target){
				const $bloque_buscar_cedula = document.querySelector('#bloque_buscar_cedula');
				const $bloque_buscar_lote = document.querySelector('#bloque_buscar_lote');
				const $bloque_buscar_nombre = document.querySelector('#bloque_buscar_nombre');
				if (target == 1) {
						$bloque_buscar_cedula.style.display = 'block';
							$bloque_buscar_lote.style.display = 'none';
							$bloque_buscar_nombre.style.display = 'none';
				} else if(target == 2){
						$bloque_buscar_lote.style.display = 'block';
						$bloque_buscar_cedula.style.display = 'none';
						$bloque_buscar_nombre.style.display = 'none';

				}else if(target == 3){
						$bloque_buscar_nombre.style.display = 'block';
						$bloque_buscar_cedula.style.display = 'none';
						$bloque_buscar_lote.style.display = 'none';

				}else if(target == 0){
						$bloque_buscar_cedula.style.display = 'none';
						$bloque_buscar_lote.style.display = 'none';
						$bloque_buscar_nombre.style.display = 'none';
				}
		}

		mostrarComprador(comprador){

				const $error_text_busqueda = document.querySelector('.error');
				$error_text_busqueda.style.display = 'none';
				let mostrar = '';
				mostrar +=`
				<tr>
					<td>${comprador.nombres}</td>
					<td>${comprador.apellidos}</td>
					<td class="level-right">
						<a href="" class="button is-info is-small" data-id='${comprador.id}'>Actualizar</a>
					</td>
				</tr>`;

				$mostrar_resultado_busqueda.innerHTML = mostrar;
		}

		openModal(){
				$modal.classList.add('is-active');
				rootEl.classList.add('is-clipped');
		}

		closeModal(){
				rootEl.classList.remove('is-clipped');
				$modal.classList.remove('is-active');
		}

		mostrarDatosModalComprador(value){
				this.cargarCompradorID(value)
				.then(comprador =>{
						if (comprador.estado_respuesta == 1) {
								this.openModal();
								let nombres           = $form_actualizar_comprador.nombres_update,
								apellidos             = $form_actualizar_comprador.apellidos_update,
								id_identificacion     = $form_actualizar_comprador.tipo_documento_update,
								numero_identificacion = $form_actualizar_comprador.numero_identificacion_update,
								direccion             = $form_actualizar_comprador.direccion_update,
								correo                = $form_actualizar_comprador.correo_update,
								celular               = $form_actualizar_comprador.celular_update,
								dia_pago              = $form_actualizar_comprador.dia_pago_update,
								id_comprador          = $form_actualizar_comprador.id_comprador_update;

								//
								nombres.value               = comprador.data['nombres'];
								apellidos.value             = comprador.data['apellidos'];
								id_identificacion.value     = comprador.data['id_identificacion'];
								numero_identificacion.value = comprador.data['numero_identificacion'];
								direccion.value             = comprador.data['direccion_residencial'];
								correo.value                = comprador.data['correo_electronico'];
								celular.value               = comprador.data['celular'];
								dia_pago.value              = comprador.data['dia_pago'];
								id_comprador.value          = comprador.data['id'];
						}
				});

		}

		mostrarCompradorFromInput(letras){
			this.cargarCompradorPorNombre(letras)
			.then(nombre => {
					const $error_text_busqueda = document.querySelector('.error');
					$error_text_busqueda.style.display = 'none';
					if (nombre.estado_respuesta == 1) {
							let mostrar = '';
							let datos = nombre.data;
							$mostrar_resultado_busqueda.innerHTML = '';
							datos.forEach(comprador =>{
									mostrar +=`
									<tr>
										<td>${comprador.nombres}</td>
										<td>${comprador.apellidos}</td>
										<td class="level-right">
											<a href="" class="button is-info is-small" data-id='${comprador.id}'>Actualizar</a>
										</td>
									</tr>`;
							});

							$mostrar_resultado_busqueda.innerHTML = mostrar;
					}
			});
		}


}

const actualizarComprador = new ActualizarComprador();

//LISTENER
document.addEventListener('DOMContentLoaded', e=>{

		$mostrar_resultado_busqueda.addEventListener('click', e=>{
				e.preventDefault();
				let id_comprador = e.target.dataset.id;
				actualizarComprador.mostrarDatosModalComprador(id_comprador);
		});

		$modal.addEventListener('click', e =>{
				if (e.target.classList.contains('cancel-update')) {
						actualizarComprador.closeModal();
				}
		});

		const $dia_pago_update = document.querySelector('#dia_pago_update');

		for (let i = 0; i < 31; i++) {

			$dia_pago_update.options[i] = new Option(i+1, (i+1));

		}

});

$select_tipo_busqueda.addEventListener('change', e=>{
		actualizarComprador.mostrarBloquesBusqueda(e.target.value);
});

$form_buscar_cedula.addEventListener('submit', e=>{
		e.preventDefault();
		let form = new FormData(e.target);

		actualizarComprador.cargarComprador(form)
		.then(comprador =>{
				if (comprador.estado_respuesta == 0) {
						Swal.fire({
							type: 'error',
							title: 'Opps..!',
							text: 'No existe este numero de cedula en la base de datos!'
						});
						return false;
				} else{
						actualizarComprador.mostrarComprador(comprador.data);
				}
		})
});

$form_buscar_lote.addEventListener('submit', e=>{
		e.preventDefault();
		let form = new FormData(e.target);

		actualizarComprador.cargarCompradorPorLote(form)
		.then(comprador =>{
				if (comprador.estado_respuesta == 0) {
						Swal.fire({
							type: 'error',
							title: 'Opps..!',
							text: 'No hay ningún comprador asociado a este lote!'
						});
						return false;
				} else {
						actualizarComprador.mostrarComprador(comprador.data);
				}
		});

});


$buscar_nombre_input.addEventListener('keyup', e =>{

			let form = new FormData();
			form.append('buscar_nombre', e.target.value);
			if (e.key !== 'Backspace' && e.target.value.length >= 3) {
					setTimeout(()=>{
							actualizarComprador.mostrarCompradorFromInput(form);
					}, 1000)
			} else{
					$mostrar_resultado_busqueda.innerHTML = '';
			}

});


$form_actualizar_comprador.addEventListener('submit', e=>{
		e.preventDefault();
		let form = new FormData(e.target);

		actualizarComprador.actualizarUnComprador(form)
		.then(comprador =>{
				if (comprador.estado_respuesta == 0) {
						Swal.fire({
							type: 'error',
							title: 'Opps..!',
							text: 'Datos sin poderse actualizar!'
						});
						return false;
				} else{
						Swal.fire({
							type: 'success',
							title: 'Genial..!',
							text: 'Datos actualizados con exito!'
						})
						.then(() =>{
								actualizarComprador.closeModal();
								location.reload();
						});
				}
		})

});

document.addEventListener('keydown', event => {
		let e = event || window.event;

		if (e.keyCode === 27) {
				actualizarComprador.closeModal();
		}
});
