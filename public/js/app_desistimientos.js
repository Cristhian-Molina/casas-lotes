import {Ajax} from './helpers/ajax.js';

const $error_mostrar_desistimientos = document.querySelector('.error');
const $total_abonados = document.querySelector('.total_abonados');
const $btn_generar_pdf = document.querySelector('#btn_generar_pdf');

const ajax = new Ajax();

class Desistimientos {

		async cargarDatosDesistimientos(){

				try{
						let peticion = await ajax.fetchData('AjaxRequest/cargar_data_desistimientos/');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		formatoNumeroMiles(n){
				return new Intl.NumberFormat('es-CO', {
					style : 'currency',
					currency : 'COP',
					maximumFractionDigits: 0,
					minimunFractionDigits : 0
				}).format(n);
		}

		quitarFormatoMoneda(moneda){

				moneda = moneda.replace("$","");
				moneda = moneda.replaceAll(".","");
				moneda = parseFloat(moneda);
				return(moneda); //returns --> 1200.51

		}

		separadorMiles (n) {

			n = String(n).replace(/\D/g, "");
			return n === '' ? n : Number(n).toLocaleString();
		}

		sumandoArray(array){

			return array.reduce((a, b) => a + b, 0);
		}

		mostrarDesistimientos(desistimientos){

			if (desistimientos.estado_respuesta == 0) {
					$error_mostrar_desistimientos.style.display = 'block';
					$btn_generar_pdf.setAttribute('disabled', 'disabled');
			} else{
					$btn_generar_pdf.removeAttribute('disabled');
					let $cargar_desistimientos = document.querySelector('#cargar_desistimientos');
					let listado = '';
					let array_total_desistimientos = [];
					let datos = desistimientos.data;
					datos.forEach(desistimiento =>{
							let tipo_lote = (desistimiento.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';
							let total_abonado = this.quitarFormatoMoneda(desistimiento.total_abonado);
							array_total_desistimientos.push(total_abonado);

							listado +=`
							<tr>
								<td>${desistimiento.fecha}</td>
								<td>${desistimiento.numero_lote}</td>
								<td>${tipo_lote}</td>
								<td>${desistimiento.nombres}</td>
								<td>${desistimiento.apellidos}</td>
								<td>${desistimiento.numero_identificacion}</td>
								<td>${desistimiento.celular}</td>
								<td>${this.formatoNumeroMiles(desistimiento.total_abonado)}</td>
							</tr>`;
					});

					$cargar_desistimientos.innerHTML = listado;
					let suma_valor_abonados = this.sumandoArray(array_total_desistimientos);
					$total_abonados.textContent = this.formatoNumeroMiles(suma_valor_abonados);
			}
		}

		generarPdfDesistimientos(desistimientos){

			let doc = new jspdf.jsPDF()
			doc.addImage({
					imageData : `${url_javascript}public/img/logo-puertas-del-sol.jpg`,
					x         : 160,
					y         : 10,
					w         : 25,
					h         : 29
			});

			doc.setFont("courier");
			doc.setFontSize(12);
			let fecha = new Date();
			let options = {year: 'numeric', month: 'long', day: 'numeric' };
			let array = [];
			let array_total_desistimientos = [];
			let array_totales = [];

			doc.setFont('Courier', 'bold');
			doc.text(`Jamundi Valle,`, 10, 60);
			doc.setFont("courier", 'normal');
			doc.text(`${fecha.toLocaleDateString("es-ES", options)}`, 48, 60)
			doc.setFontSize(12);
			doc.setFont('Courier', 'bold');
			doc.text('ESTIMADO DE LOTES DESISTIDOS', 100, 80, {align: 'center'});

			desistimientos.forEach(desistimiento =>{

					let tipo_lote = (desistimiento.id_tipo_lote == 1) ? 'Viv.' : 'Com.';
					let total_abonado = this.quitarFormatoMoneda(desistimiento.total_abonado);

					array_total_desistimientos.push(total_abonado);

					array.push([desistimiento.fecha, desistimiento.numero_lote, tipo_lote, desistimiento.nombres, desistimiento.apellidos, this.separadorMiles(desistimiento.numero_identificacion), desistimiento.celular, this.formatoNumeroMiles(desistimiento.total_abonado)]);

			});

			let suma_valor_lotes = this.sumandoArray(array_total_desistimientos);

			array_totales.push([this.formatoNumeroMiles(suma_valor_lotes)]);

			let head_total = [['Valor Total Desistimientos']];
			let body_total = array_totales;
			doc.autoTable({
				head: head_total,
				body: body_total,startY: 95,
				theme: 'grid',
				styles: {
						halign: 'center'
				}
			});

			let head = [['Fecha', '# Lote', 'Tipo Lote','Nombres', 'Apellidos', 'Cédula', 'celular', 'Total Abonado']]
			let body = array;
			doc.autoTable({
				head: head,
				body: body ,
				startY: 120,
				styles:{
					halign: 'center'
				}
			});
			window.open(doc.output('bloburl'), '_blank');
		}
}

const desistir = new Desistimientos();

document.addEventListener('DOMContentLoaded', e=>{

		desistir.cargarDatosDesistimientos()
		.then(desistimientos =>{
				desistir.mostrarDesistimientos(desistimientos);
		});

});

$btn_generar_pdf.addEventListener('click', e =>{

	desistir.cargarDatosDesistimientos()
	.then(resp =>{
			desistir.generarPdfDesistimientos(resp.data);
	});
});

