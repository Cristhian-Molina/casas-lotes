import {Ajax} from './helpers/ajax.js';

const $cargar_lotes_vivienda    = document.querySelector('#cargar_lotes_vivienda');
const $cargar_lotes_comercial   = document.querySelector('#cargar_lotes_comercial');
const $error_lotes_vivienda     = document.querySelector('.error-lotes-vivienda');
const $error_lotes_comerciales  = document.querySelector('.error-lotes-comerciales');
const $form_nuevo_lote          = document.querySelector('#form_nuevo_lote');
const rootEl                    = document.documentElement;
const $modal                    = document.querySelector('.modal');
const $form_actualizar_lotes         = document.querySelector('#form_actualizar_lotes');
const $contenido_tablas_lotes = document.querySelector('.contenido_tablas_lotes');

const $alerta_mora = document.querySelector('#alerta_mora');


//INSTANCIA DE LA CLASE AJAX
const ajax = new Ajax();

class Lotes{

	async cargarLotesVivienda(){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/cargar_lotes_vivienda/');
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}

	async cargarLotesComercial(){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/cargar_lotes_comercial/');
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}

	async enviarNuevoLote(form){
			try{
					let peticion = await ajax.fetchData('AjaxRequest/agregar_nuevo_lote/', form, 'POST');
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}

	async cargarDataUnLote(id, value){

			try	{
					let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_lote_data_id/${id}/${value}`);
					return peticion;
			} catch(e){
					console.log('ERROR =>', e);
			}
	}

	async actualizarDataLote(form){

			try	{
					let peticion = await ajax.fetchData(`AjaxRequest/actualizar_un_lote_data/`, form, 'POST');
					return peticion;
			} catch(e){
					console.log('ERROR =>', e);
			}
	}

	async actualizarEstadoLote(id, value){

			try	{
					let peticion = await ajax.fetchData(`AjaxRequest/actualizar_estado_lote/${id}/${value}`);
					return peticion;
			} catch(e){
					console.log('ERROR =>', e);
			}
	}

	async desistirLote(id_lote){

		try	{
				let peticion = await ajax.fetchData(`AjaxRequest/desistir_lote/${id_lote}`);
				return peticion;
		} catch(e){
				console.log('ERROR =>', e);
		}

	}

	async cargarUltimoPagosLotes(){

		try{
			let peticion = await ajax.fetchData(`AjaxRequest/cargar_ultimo_pago_lotes`);
			return peticion;
		} catch(e){
			console.log('ERROR => ', e);
		}
	}


	mostrarLotesVivienda(lotes){
			if(lotes.estado_respuesta == 0){
					$error_lotes_vivienda. style.display = 'block';
			} else{
					let listado = '';
					let estado_lote = '';
					let botones = '';
					let datos = lotes.data;
					datos.forEach(lote =>{
							if (lote.estado_lote == 1) {
									estado_lote = '<strong class="has-text-success">Disponible</strong>';
							} else if(lote.estado_lote == 2){
									 estado_lote = '<strong class="has-text-warning">Abonado</strong>';
							} else if(lote.estado_lote == 3){
									 estado_lote = '<strong class="has-text-info">Separado</strong>';
							} else if(lote.estado_lote == 4){
									 estado_lote = '<strong class="has-text-danger">Pagado</strong>';
							}

							if (lote.estado_lote == 1) {
										botones =
										`<a class="button is-info is-small modal-update-vivienda" data-id="${lote.id}">Actualizar</a>`
							}

							if (lote.estado_lote == 2) {
									botones =
									`<a href="${url_javascript}comprador/abona_lote/${lote.id}" class="button is-link is-small modal-abonar-vivienda" data-id="${lote.id}">Abonar lote</a>
									<a class="button is-danger is-small desistir-vivienda"data-id="${lote.id}">Desistir</a>`
							}

							if (lote.estado_lote == 3) {
									botones = `<a class="button is-info is-small modal-update-vivienda is-fullwidth" data-id="${lote.id}">Actualizar</a>
										<a href="${url_javascript}comprador/abona_lote/${lote.id}" class="button is-warning is-small is-fullwidth" data-id="${lote.id}">Recalcular</a>`;
							}

							if (lote.estado_lote == 4) {
									botones = `<a href="${url_javascript}lotes/hoja_lote/${lote.id}" class="button is-primary is-small is-fullwidth" data-id="${lote.id}">Ver hoja de vida</a>`
							}


							listado +=
							`<tr>
								<td width="5%">${lote.numero_lote}</td>
								<td>${lote.medidas} mts</td>
								<td>${lote.valor}</td>
								<td>${estado_lote}</td>
								<td class="level-right">${botones}</td>
							</tr>`
					});
					$cargar_lotes_vivienda.innerHTML = listado;
			}
	}

	mostrarLotesComercial(lotes){
			if(lotes.estado_respuesta == 0){
					$error_lotes_comerciales.style.display = 'block';
			} else{
					let listado = '';
					let estado_lote = '';
					let botones = '';
					let datos = lotes.data;
					datos.forEach(lote =>{

							if (lote.estado_lote == 1) {
									estado_lote = '<strong class="has-text-success">Disponible</strong>';
							} else if(lote.estado_lote == 2){
									 estado_lote = '<strong class="has-text-warning">Abonado</strong>';
							} else if(lote.estado_lote == 3){
									 estado_lote = '<strong class="has-text-info">Separado</strong>';
							} else if(lote.estado_lote == 4){
									 estado_lote = '<strong class="has-text-danger">Pagado</strong>';
							}


							if (lote.estado_lote == 1) {
										botones =
										`<a class="button is-info is-small modal-update-comercial" data-id="${lote.id}">Actualizar</a>`
							}

							if (lote.estado_lote == 2) {
									botones =
									`<a href="${url_javascript}comprador/abona_lote/${lote.id}" class="button is-link is-small modal-abonar-comercial" data-id="${lote.id}">Abonar lote</a>
									<a class="button is-danger is-small desistir-comercial"data-id="${lote.id}">Desistir</a>`
							}

							if (lote.estado_lote == 3) {
									botones = `<a class="button is-info is-small modal-update-comercial is-fullwidth" data-id="${lote.id}">Actualizar</a>
										<a href="${url_javascript}comprador/abona_lote/${lote.id}" class="button is-warning is-small is-fullwidth" data-id="${lote.id}">Recalcular</a>`;
							}

							if (lote.estado_lote == 4) {
									botones = `<a href="${url_javascript}lotes/hoja_lote/${lote.id}" class="button is-primary is-small is-fullwidth" data-id="${lote.id}">Ver hoja de vida</a>`
							}

							listado +=
							`<tr>
								<td width="5%">${lote.numero_lote}</td>
								<td>${lote.medidas} mts</td>
								<td>${lote.valor}</td>
								<td>${estado_lote}</td>
								<td class="level-right">${botones}</td>
							</tr>`
					});
					$cargar_lotes_comercial.innerHTML = listado;
			}
	}


	mostrarDataLote(target, value){

			this.cargarDataUnLote(target, value)
			.then(lote =>{
					//console.log(lote.data);
					if (lote.estado_respuesta == 1) {

							this.openModal();

							let medidas = $form_actualizar_lotes.medidas_update,
							valor = $form_actualizar_lotes.valor_update,
							tipo_lote = $form_actualizar_lotes.valor_tipo_lote,
							numero_lote = $form_actualizar_lotes.numero_lote;

							medidas.value = lote.data['medidas'];
							valor.value = lote.data['valor'];
							tipo_lote.value = lote.data['id_tipo_lote'];
							numero_lote.value = lote.data['numero_lote'];

					}
			});

	}

	mostrarAlertasMora(alertas){
		if (alertas.estado_respuesta == 1) {

				$alerta_mora.style.display = 'block';
		}
	}


	validarDatosLote(){
		let tipo_lote = $form_nuevo_lote.tipo_lote.selectedIndex;
		let numero_lote = $form_nuevo_lote.numero_lote;
		let medidas_lote = $form_nuevo_lote.medidas_lote;
		let valor_lote = $form_nuevo_lote.valor_lote;

		if (tipo_lote == null || tipo_lote == 0) {
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer no seleccionaste el tipo del lote!'
				});
				return false;
		} else if(numero_lote.value == ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer el numero de lote esta vacio!'
				});
				return false;
		} else if(medidas_lote.value == ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer la medida del lote esta vacio!'
				});
				return false;
		} else if(valor_lote.value== ''){
				Swal.fire({
						type: 'error',
						title: 'Opps..',
						text: 'Al parecer el valor de lote esta vacio!'
				});
				return false;
		}
		return true;
	}

	formatoNumeroMiles(n){
			return new Intl.NumberFormat('es-CO', {
				style : 'currency',
				currency : 'COP',
				minimunFractionDigits : 0,
				maximumFractionDigits: 0,
			}).format(n);
	}

	openModal(){

			$modal.classList.add('is-active');
			rootEl.classList.add('is-clipped');
	}

	closeModal(){

			rootEl.classList.remove('is-clipped');
			$modal.classList.remove('is-active');
	}



}//END CLASS LOTES


const lotes = new Lotes();


document.addEventListener('DOMContentLoaded', () =>{
		lotes.cargarLotesVivienda()
		.then(viviendas =>{
			lotes.mostrarLotesVivienda(viviendas)
		});

		lotes.cargarLotesComercial()
		.then(comerciales =>{
			lotes.mostrarLotesComercial(comerciales)
		});


		//CONTENIDO GLOBAL QUE CONTIENE LAS TABLAS
		$contenido_tablas_lotes.addEventListener('click', e=>{
				let id = e.target.dataset.id;

				if (e.target.classList.contains('modal-update-vivienda')) {

						//CONSULTAR LA INFORMACION Y MOSTRAR EN EL MODAL
						lotes.mostrarDataLote(id, 1);
				}

				if (e.target.classList.contains('modal-update-comercial')) {

						//CONSULTAR LA INFORMCACION Y MOSTRAR EN EL MODAL
						lotes.mostrarDataLote(id, 2);
				}

				if (e.target.classList.contains('desistir-vivienda')) {

						Swal.fire({
								title: 'Estas seguro en hacer el desistimiento de este lote de vivienda?',
								text: "No se podra revertir esto!",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Si, desistelo!'
						})
						.then((result) =>{
								if (result.value) {
										lotes.desistirLote(id)
										.then(desistir =>{
											//console.log(desistir);
												if (desistir.estado_respuesta == 1) {
															Swal.fire({
															title:'Desistimiento!',
															text:'El lote fue desistido',
															type:'success'
														})
														.then(()=>{
																location.reload();
														})
												}
										})
								}
						});

				}

				if (e.target.classList.contains('desistir-comercial')) {

						Swal.fire({
								title: 'Estas seguro en hacer el desistimiento de este lote comercial?',
								text: "No se podra revertir esto!",
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Si, desistelo!'
						})
						.then(result =>{
								if (result.value) {
										lotes.desistirLote(id)
										.then(desistir =>{
												if (desistir.estado_respuesta == 1) {
															Swal.fire({
															title:'Desistimiento!',
															text:'El lote fue desistido',
															type:'success'
														})
														.then(()=>{
																location.reload();
														})
												}
										})
								}
						});
				}


		}); //END LISTENER CLICK =>	$contenido_talas_lotes


		$modal.addEventListener('click', e =>{
				if (e.target.classList.contains('cancel-update')) {
						lotes.closeModal();
				}
		});

		//LOGICA QUE MUESTRA LAS ALERTAS DE LOS LOTES QUE ESTAN EN MORA
		lotes.cargarUltimoPagosLotes()
		.then(lotes => {

			let lista = '';

			lotes.data.forEach((sn) =>{

				lista +=`
				<article class="message is-danger is-small">
					<div class="message-body">
						Señor(a) <strong>${sn.nombres} ${sn.apellidos}</strong>, con lote numero ${sn.numero_lote}, de tipo ${sn.tipo_lote} se encuentra en mora, su numero de contacto es <strong>${sn.celular}</strong> ultima fecha de abono <strong>${sn.ultimo_pago}</strong> <a href="${url_javascript}comprador/abona_lote/${sn.id_lote}"> Clic para ir a la hoja del lote</a>
					</div>
				</article>`;
			});

			$alerta_mora.innerHTML = lista;

		})


});//END DOMCONTENTLOADED


//LISTENER
$form_nuevo_lote.addEventListener('submit', e =>{
		e.preventDefault();
		lotes.validarDatosLote();
		if (lotes.validarDatosLote()) {
				let form = new FormData(form_nuevo_lote);

				lotes.enviarNuevoLote(form)
				.then(data => {
						if (data.estado_respuesta == 0) {
								Swal.fire({
									type: 'error',
									title: 'Opps..!',
									text: 'Datos sin poderse guardar!'
								});
								return false;
						} else if(data.estado_respuesta == 1){
								Swal.fire({
									type: 'success',
									title: 'Genial..!',
									text: 'Datos Guardados con exito!'
								});
						}
				})
				.then(() =>{
						$form_nuevo_lote.reset();
						lotes.cargarLotesVivienda()
						.then(vivienda =>{
								$error_lotes_vivienda.style.display = 'none';
								lotes.mostrarLotesVivienda(vivienda);
						});
						lotes.cargarLotesComercial()
						.then(comercial =>{
								$error_lotes_comerciales.style.display = 'none';
								lotes.mostrarLotesComercial(comercial);
						})

				});
		}
});

$form_nuevo_lote.valor_lote.addEventListener('change', e =>{
		const element = e.target;
		const value = element.value;
		element.value = lotes.formatoNumeroMiles(value);
});

$form_actualizar_lotes.valor_update.addEventListener('change', e=>{
		const element = e.target;
		const value = element.value;
		element.value = lotes.formatoNumeroMiles(value);
});

$form_actualizar_lotes.addEventListener('submit', e=>{
		e.preventDefault();
		let form = new FormData(e.target);
		lotes.actualizarDataLote(form)
		.then(actualizar =>{
				if(actualizar.estado_respuesta == 1){
						Swal.fire({
							type: 'success',
							title: 'Genial..!',
							text: 'Datos actualizados con exito!'
						})
						.then(() =>{
								lotes.closeModal();
								location.reload();
						});
				}
		});

})

document.addEventListener('keydown', event => {
		let e = event || window.event;

		if (e.keyCode === 27) {
				lotes.closeModal();
		}
});

