import {Ajax} from './helpers/ajax.js';

const $form_calcular_lote = document.querySelector('#form_calcular_lote');
const $lote_cuotas = document.querySelector('#lote_cuotas');
const $valor_interes = document.querySelector('#valor_interes');
const $valor_lote = document.querySelector('#valor_lote');

const ajax = new Ajax();

class CalcularSeparado{

		async guardarDatosSeparado(form){

				try{
						let peticion = await ajax.fetchData('AjaxRequest/guardar_abono_lote_separado/', form, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		showPikaday(element){

				var picker = new Pikaday({
						field: document.getElementById(element),
						format: 'YYYY-MM/DD',
						i18n: {
								previousMonth : 'Anterior',
								nextMonth     : 'Siguiente',
								months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
								weekdays      : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
								weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
						},
						toString(date, format) {
									// you should do formatting based on the passed format,
									// but we will just return 'D/M/YYYY' for simplicity
									let day = date.getDate();
									let month = date.getMonth() + 1;
									let year = date.getFullYear();
										if(day < 10){
												day = '0'+ day; //agrega cero si el menor de 10
										}
										if(month < 10){
												month = '0' + month //agrega cero si el menor de 10
										}
									return `${year}-${month}-${day}`;
						},
						parse(dateString, format) {
								// dateString is the result of `toString` method
								const parts = dateString.split('-');
								const day = parseInt(parts[0], 10);
								const month = parseInt(parts[1], 10) - 1;
								const year = parseInt(parts[2], 10);
								return new Date(year, month, day);
						}
				});

				return picker;
		}

		mostarNumeroCuotas(){
				const $lote_cuotas = $form_calcular_lote.lote_cuotas;
				let listado = '';
				listado +='<option value="">Seleccione...</option>';

				for (let i=1; i <=60; i++) {
						listado += `<option value="${i}">${i}</option>`;
				}

				$lote_cuotas.innerHTML = listado;
		}

		formatoNumeroMiles(n){

				return new Intl.NumberFormat('es-CO', {
					style : 'currency',
					currency : 'COP',
					minimunFractionDigits : 0
				}).format(n);
		}

		quitarFormatoMoneda(moneda){

				moneda = moneda.replace("$","");
				moneda = moneda.replaceAll(".","");
				moneda = parseFloat(moneda);
				return(moneda); //returns --> 1200.51

		}

		validarAbonoSeparado(){
			let valor_interes = $form_calcular_lote.valor_interes;
			let lote_cuotas = $form_calcular_lote.lote_cuotas.selectedIndex;
			let recibo_abono = $form_calcular_lote.recibo_abono;
			let tipo_abono = $form_calcular_lote.tipo_abono.selectedIndex;
			let fecha_abono = $form_calcular_lote.fecha_abono;
			let concepto_abono = $form_calcular_lote.concepto_abono;


			if (valor_interes.value == '') {
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del % de la cuota inicial esta vacia!'
					});
					return false;
			} else if(lote_cuotas == null || lote_cuotas == 0){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer no elegiste un numero de coutas!'
					});
					return false;
			} else if(recibo_abono.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del recibo esta vacio!'
					});
					return false;
			} else if(tipo_abono == null || tipo_abono == 0){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer no elegiste el tipo del abono!'
					});
					return false;
			} else if(fecha_abono.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo de la fecha esta vacio!'
					});
					return false;
			} else if(concepto_abono.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del concepto para el abono esta vacio!'
					});
					return false;
			}
			return true;
		}

}

const separado = new CalcularSeparado();

document.addEventListener('DOMContentLoaded', e=>{

		separado.mostarNumeroCuotas();
		separado.showPikaday('fecha_abono');
});

$valor_interes.addEventListener('change', e =>{
		let interes = parseInt(e.target.value);
		//let valor_lote_formateado = parseInt($valor_lote.value.replaceAll('.', ''));
		let valor_lote_formateado = separado.quitarFormatoMoneda($valor_lote.value);
		//let prueba= quitarFormatoMoneda(valor_lote_formateado);
		let valor_porcentaje = interes / 100;

		//CALCULAR EL VALOR DE LA CUOTA INICIAL 
		let cuota_inicial = (valor_lote_formateado * valor_porcentaje);
		$form_calcular_lote.valor_abonado.value = separado.formatoNumeroMiles(cuota_inicial);
		//let abono = parseInt($form_agregar_lote_comprador.abono.value.replaceAll('.', ''));
		let abono = separado.quitarFormatoMoneda($form_calcular_lote.valor_abonado.value);

		let total_deuda = valor_lote_formateado - abono;
		//REEMPLAZAR EL VALOR DEL CAMPO ABONO POR EL 
		$form_calcular_lote.total_deuda.value = separado.formatoNumeroMiles(total_deuda);
});

$lote_cuotas.addEventListener('change', e =>{

		const form = $form_calcular_lote;
		let cuota = parseInt(e.target.value);
		let total_deuda = separado.quitarFormatoMoneda(form.total_deuda.value);


		let interes = form.valor_interes.value / 100;
		let valor_por_cuota = parseInt(form.valor_por_cuota.value);

		if (cuota <= 12) {

				let sin_interes = total_deuda / 12;
				form.valor_por_cuota.value = separado.formatoNumeroMiles(sin_interes);
		} else {
				
				let total_cuota_a_mes = total_deuda /( ( 1- (1 + 0.01) ** (-cuota)) / 0.01);

				//form.valor_por_cuota.value = separado.formatoNumeroMiles(Math.round(total_cuota_a_mes));
				form.valor_por_cuota.value = separado.formatoNumeroMiles(total_cuota_a_mes);
		}

		let abono_formateado = separado.quitarFormatoMoneda(form.valor_abonado.value);
		let valor_por_cuota_formateada = separado.quitarFormatoMoneda(form.valor_por_cuota.value);

		let valor_total_financiado = (valor_por_cuota_formateada * cuota) + abono_formateado;

		form.total_lote_financiado.value = separado.formatoNumeroMiles(valor_total_financiado);
});

$form_calcular_lote.addEventListener('submit', e=>{

		e.preventDefault();
		if (separado.validarAbonoSeparado()) {
				let form = new FormData(e.target);
				separado.guardarDatosSeparado(form)
				.then(data =>{
					if (data.estado_respuesta == 1) {
							Swal.fire({
								type: 'success',
								title: 'Genial..!',
								text: 'Abono ingresado con exito!'
							})
							.then(() =>{
									window.location.href = `${url_javascript}comprador/abona_lote/${$form_calcular_lote.id_lote.value}`;
							});
					}
				});
		}
});


$form_calcular_lote.valor_abonado.addEventListener('change', e => {

		const element = e.target;
		const value = element.value;
		element.value = separado.formatoNumeroMiles(value);
		const recal = separado.quitarFormatoMoneda($valor_lote.value) - separado.quitarFormatoMoneda(value);
		//console.log(recal);
		$form_calcular_lote.total_deuda.value = separado.formatoNumeroMiles(recal)
});