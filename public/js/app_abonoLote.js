import {Ajax} from './helpers/ajax.js';

const $form_agregar_nuevo_abono = document.querySelector('#form_agregar_nuevo_abono');
const $cargar_detalle_abonos    = document.querySelector('#cargar_detalle_abonos');
const rootEl                    = document.documentElement;
const $modal                    = document.querySelector('.modal');
const $form_actualizar_abonos   = document.querySelector('#form_actualizar_abonos');
const $btn_generar_pdf    = document.querySelector('#btn_generar_pdf');

const $suma_abonos = document.querySelector('#suma_abonos');

let total_lote = document.querySelector('#valor_lote');

const ajax = new Ajax();

class AbonoLote{

		async guardarAbono(form){

				try{
						let peticion = await ajax.fetchData('AjaxRequest/guardar_abono_lote/', form, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async cargarDetalleAbonos(){

				try{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_detalles_abonos/${form_agregar_nuevo_abono.id_lote.value}`);
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async cargarUnAbono(id_abono){
				try	{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_un_abono/${id_abono}`);
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}

		async actualizarFormAbono(form){
				try{
						let peticion = await ajax.fetchData('AjaxRequest/actualizar_un_abono', form, 'POST');
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}


		async eliminarUnAbono(id_abono){
				try{
						let peticion = await ajax.fetchData(`AjaxRequest/eliminar_un_abono/${id_abono}`);
						return peticion;
				} catch(e){
						console.log('ERROR =>', e);
				}
		}


		validarAbono(){
			let fecha         = $form_agregar_nuevo_abono.fecha_abono;
			let concepto      = $form_agregar_nuevo_abono.concepto_abono;
			let recibo        = $form_agregar_nuevo_abono.recibo_abono
			let tipo_abono    = $form_agregar_nuevo_abono.tipo_abono.selectedIndex;
			let valor_abonado = $form_agregar_nuevo_abono.valor_abonado;

			if(fecha.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo de la fecha esta vacia!'
					});
					return false;
			} else if(concepto.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del concepto esta vacio!'
					});
					return false;
			}else if(recibo.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del recibo est vacio!'
					});
					return false;
			}else if(tipo_abono == null ||tipo_abono == 0){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer no seleccionaste un tipo de abono!'
					});
					return false;
			}else if(valor_abonado.value == ''){
					Swal.fire({
							type: 'error',
							title: 'Opps..',
							text: 'Al parecer el campo del valor del abono esta vacio!'
					});
					return false;
			}

			return true;
		}

		showPikaday(element){

				var picker = new Pikaday({
						field: document.getElementById(element),
						format: 'YYYY-MM/DD',
						i18n: {
								previousMonth : 'Anterior',
								nextMonth     : 'Siguiente',
								months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
								weekdays      : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
								weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
						},
						toString(date, format) {
									// you should do formatting based on the passed format,
									// but we will just return 'D/M/YYYY' for simplicity
									let day = date.getDate();
									let month = date.getMonth() + 1;
									let year = date.getFullYear();
										if(day < 10){
												day = '0'+ day; //agrega cero si el menor de 10
										}
										if(month < 10){
												month = '0' + month //agrega cero si el menor de 10
										}
									return `${year}-${month}-${day}`;
						},
						parse(dateString, format) {
								// dateString is the result of `toString` method
								const parts = dateString.split('-');
								const day = parseInt(parts[0], 10);
								const month = parseInt(parts[1], 10) - 1;
								const year = parseInt(parts[2], 10);
								return new Date(year, month, day);
						}
				});

				return picker;
		}

		formatoNumeroMiles(n){
				return new Intl.NumberFormat('es-CO', {
					style : 'currency',
					currency : 'COP',
					maximumFractionDigits: 0,
					minimunFractionDigits : 0
				}).format(n);
		}

		quitarFormatoMoneda(moneda){

				moneda = moneda.replace("$","");
				moneda = moneda.replaceAll(".","");
				moneda = parseFloat(moneda);
				return(moneda); //returns --> 1200.51

		}

		separadorMiles (n) {

			n = String(n).replace(/\D/g, "");
			return n === '' ? n : Number(n).toLocaleString();
		}

		mostrarDetalleAbonos(obj, total_lote){

				let listado = '';
				let tipo_abono = '';
				let total = this.quitarFormatoMoneda(total_lote);
				obj.forEach(abono =>{

						if (abono.forma_abono_comprador == 1) {
								tipo_abono = 'Efectivo';
						} else if(abono.forma_abono_comprador == 2){
								tipo_abono = 'Bancolombia - #2175';
						} else if(abono.forma_abono_comprador == 3){
								tipo_abono = 'Bancolombia - #1063'
						} else if(abono.forma_abono_comprador == 4){
								tipo_abono = 'Banco Occidente - #6391'
						} else if(abono.forma_abono_comprador == 5){
								tipo_abono = 'Banco Occidente - #6383'
						}
						total = (total - this.quitarFormatoMoneda(abono.valor_abono));

						listado += `
						<tr>
							<td>${abono.fecha}</td>
							<td>${abono.concepto}</td>
							<td>${abono.numero_recibo}</td>
							<td>${tipo_abono}</td>
							<td>${abono.observaciones}</td>
							<td>${abono.valor_abono}</td>
							<td>${this.formatoNumeroMiles(total)}</td>
							<td>
							<a class="button is-info is-small modal-update-abono" data-id="${abono.id}">Actualizar</a>
							<a class="button is-danger is-small modal-delete-abono" data-id="${abono.id}">Eliminar</a>
							</td>
						</tr>`;
				});
				$cargar_detalle_abonos.innerHTML = listado;
		}

		mostrarUnAbono(target){

				this.cargarUnAbono(target)
				.then(abono =>{
						console.log(abono);

						this.openModal();

						//TREAEMOS LOS CAMPOS DEL MODAL DEL ABONO
						let recibo_abono = $form_actualizar_abonos.recibo_abono_update;
						let tipo_abono = $form_actualizar_abonos.tipo_abono_update;
						let id_abono =$form_actualizar_abonos.id_abono_update;
						let valor_abono = $form_actualizar_abonos.valor_abono_update;
						let observaciones = $form_actualizar_abonos.observaciones_update;

						//CARGAMOS LOS VALORES DE LA BASE DE DATOS EN EL MODAL PARA ACTUALIZAR
						recibo_abono.value          = abono.data.numero_recibo;
						tipo_abono.value        = abono.data.forma_abono_comprador;
						id_abono.value = abono.data.id;
						valor_abono.value = abono.data.valor_abono;
						observaciones.value = abono.data.observaciones;
				})
		}

		openModal(){

				$modal.classList.add('is-active');
				rootEl.classList.add('is-clipped');
		}

		closeModal(){
			rootEl.classList.remove('is-clipped');
			$modal.classList.remove('is-active');
		}

		generarPdfAbonos(abonos){

			let nombre = document.querySelector('#nombre_comprador');
			let apellidos = document.querySelector('#apellidos_comprador');
			let numero_cedula = document.querySelector('#numero_cedula');
			let tipo_lote  = document.querySelector('#tipo_lote');
			let numero_lote = document.querySelector('#numero_lote');
			let total = this.quitarFormatoMoneda(document.querySelector('#valor_lote').value);

			//let tipo_lotes = (tipo_lote.value == 1) ? 'Vivienda' : 'Comercial';

			let doc = new jspdf.jsPDF()
			doc.addImage({
					imageData : `${url_javascript}public/img/logo-puertas-del-sol.jpg`,
					x         : 160,
					y         : 10,
					w         : 25,
					h         : 29
			});

			doc.setFont("courier");
			doc.setFontSize(12);
			let fecha = new Date();
			let options = {year: 'numeric', month: 'long', day: 'numeric' };

			doc.setFont('Courier', 'bold');
			doc.text(`Jamundi Valle,`, 10, 60);
			doc.setFont("courier", 'normal');
			doc.text(`${fecha.toLocaleDateString("es-ES", options)}`, 48, 60)
			doc.setFontSize(11);
			doc.text(`Señor(a), ${nombre.value} ${apellidos.value}`, 10, 70);
			doc.text(`C.c : ${this.separadorMiles(numero_cedula.value)}`, 10, 75);

			doc.setFontSize(12);
			doc.setFont('Courier', 'bold');
			doc.text('ASUNTO :', 10, 95);
			doc.setFont("courier", 'normal');
			doc.text(`Estado de cuenta de abonos realizados hasta la fecha del lote`, 30, 95);
			doc.text(`${tipo_lote.value} # ${numero_lote.value}, valor del lote ${total_lote.value}, para un saldo total de abonos`, 10, 100);

			let tipo_abono = '';
			let array = [];
			let suma_abonos = [];
			abonos.forEach((abono)=>{
					
					if (abono.forma_abono_comprador == 1) {
							tipo_abono = 'Efectivo';
					} else if(abono.forma_abono_comprador == 2){
							tipo_abono = 'Bancolombia';
					} else if(abono.forma_abono_comprador == 3){
							tipo_abono = 'Bancolombia'
					} else if(abono.forma_abono_comprador == 4){
							tipo_abono = 'Banco Occidente'
					} else if(abono.forma_abono_comprador == 5){
							tipo_abono = 'Banco Occidente'
					}
					total = (total - this.quitarFormatoMoneda(abono.valor_abono));

					array.push([abono.fecha, abono.concepto, abono.numero_recibo, tipo_abono, abono.valor_abono, this.formatoNumeroMiles(total)]);

					//RECORRIENDO TODOS LOS ABONOS
					let limpiar = this.quitarFormatoMoneda(abono.valor_abono);
					suma_abonos.push(limpiar);


			});

			let suma = suma_abonos.reduce((a,b ) => a+b,0);

			doc.text(`${this.formatoNumeroMiles(suma)}`,10,105);

			let head = [['Fecha', 'Concepto', 'Recibo','Tipo Abono', 'Valor del Abono', 'Saldo']]
			let body = array;
			doc.autoTable({ head: head, body: body , startY: 110});

			window.open(doc.output('bloburl'), '_blank');
		}


}

const abono = new AbonoLote();

document.addEventListener('DOMContentLoaded', e =>{

		abono.showPikaday('fecha_abono');

		abono.cargarDetalleAbonos()
		.then(abonos =>{
				if (abonos.estado_respuesta == 1) {

						abono.mostrarDetalleAbonos(abonos.data, valor_lote.value);

						let array_suma_abonos = [];
						let todo_abonos = abonos.data;
						todo_abonos.forEach((ab) => {
								let limpiar = abono.quitarFormatoMoneda(ab.valor_abono);
								array_suma_abonos.push(limpiar);
						});

						let suma =array_suma_abonos.reduce((a,b ) => a+b,0);
						$suma_abonos.innerHTML = abono.formatoNumeroMiles(suma);

				}
		});

		$cargar_detalle_abonos.addEventListener('click', e=>{

				let id_abono = e.target.dataset.id;

				if (e.target.classList.contains('modal-update-abono')) {
						abono.mostrarUnAbono(id_abono);
				}

				if (e.target.classList.contains('modal-delete-abono')) {
					Swal.fire({
							title: 'Estas seguro en Eliminar Este Abono?',
							text: "No se podra revertir esto!",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Si, eliminalo!'
					})
					.then(result =>{
							if (result.value) {
									abono.eliminarUnAbono(e.target.dataset.id)
									.then(dato => {
											if (dato.estado_respuesta == 1) {
														Swal.fire({
														title:'Eliminado!',
														text:'El Abono fue eliminado',
														type:'success'
													})
													.then(()=>{
															location.reload();
													})
											}
									});

							}
					})
				}
		});

		$modal.addEventListener('click', e =>{
				if (e.target.classList.contains('cancel-update')) {
						abono.closeModal();
				}
		});

});

$form_agregar_nuevo_abono.addEventListener('submit', e =>{

		e.preventDefault();
		if (abono.validarAbono()) {
				let form = new FormData(e.target);

				abono.guardarAbono(form)
				.then(ab =>{
						if (ab.estado_respuesta == 1) {
								Swal.fire({
									type: 'success',
									title: 'Genial..!',
									text: 'Abono ingresado con exito!'
								})
								.then(() =>{
										location.reload();
								});
						}
				});
		}

});

$form_agregar_nuevo_abono.valor_abonado.addEventListener('change', e =>{

		const element = e.target;
		const value = element.value;
		element.value = abono.formatoNumeroMiles(value);
});

$form_actualizar_abonos.valor_abono_update.addEventListener('change', e =>{

		const element = e.target;
		const value = element.value;
		element.value = abono.formatoNumeroMiles(value);
});

$form_actualizar_abonos.addEventListener('submit', e=>{

		e.preventDefault();
		let form = new FormData(e.target);
		abono.actualizarFormAbono(form)
		.then(update =>{
				if (update.estado_respuesta == 1) {
						Swal.fire({
								type: 'success',
								title: 'Genial..!',
								text: 'Datos actualizados con exito!'
						})
						.then(() =>{
								abono.closeModal();
								location.reload();
						});
				}else{
						Swal.fire({
								type: 'error',
								title: 'Opps..!',
								text: 'Datos sin poderse actualizar!'
						});
						return false;
				}
		});

});

$btn_generar_pdf.addEventListener('click', e =>{

	abono.cargarDetalleAbonos()
	.then(resp =>{
			abono.generarPdfAbonos(resp.data);
	});
});

document.addEventListener('keydown', event => {
		let e = event || window.event;

		if (e.keyCode === 27) {
				vendedor.closeModal();
		}
});
