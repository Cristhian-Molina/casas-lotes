import {Ajax} from './helpers/ajax.js';

const $form_login_user = document.querySelector('#form_login_user');
//INSTANCIA DE LA CLASE Ajax
const ajax = new Ajax();


class Login{

	async sendFormLogin(form){

		try{
				let peticion = await ajax.fetchData('AjaxRequest/validar_form_login_user/', form, 'POST');
				return peticion;
		} catch(e){
			console.log('ERROR =>', e);
		}
	}

}//END CLASS LOGIN


//INSTANCIA CLASE LOGIN
const login = new Login();


//ESCUCHADOR AL ENVIAR LOS DATOS DEL FORMULARIO
$form_login_user.addEventListener('submit', e =>{
	e.preventDefault();
	let form = new FormData($form_login_user);

	login.sendFormLogin(form)
	.then((data) =>{
		if(data.estado_respuesta == 0){
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'El usuario en la base de datos no existe'
				});
				return false
		}else if(data.estado_clave == 0){
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'La contraseña con coincide'
				});
				return false
		}else if(data.estado_respuesta == 1 && data.estado_clave == 1){
				Swal.fire({
					type: 'success',
					title: 'Genial..!',
					text: 'En un momento te vamos a redirigir a la plataforma'
				})
				.then(() =>{
							window.location.href = url_javascript + 'dashboard/';
					});
		}
	})
})
