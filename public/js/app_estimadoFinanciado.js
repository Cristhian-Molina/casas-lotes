import {Ajax} from './helpers/ajax.js';

const $cargar_estimado_financiado = document.querySelector('.cargar_estimado_financiado');
const $error_mostrar = document.querySelector('.error');
const $total_lotes   = document.querySelector('.total_lotes');
const $total_ingreso = document.querySelector('.total_ingreso');
const $total_interes = document.querySelector('.total_interes');
const $btn_generar_pdf = document.querySelector('#btn_generar_pdf');

const ajax = new Ajax();

class Financiado{

	async cargarEstimadoFinanciado(){
		try{
				let peticion = await ajax.fetchData('AjaxRequest/cargar_estimado_financiado/');
				return peticion;
		} catch(e){
			console.log('ERROR =>', e);
		}
	}

	formatoNumeroMiles(n){
			return new Intl.NumberFormat('es-CO', {
				style : 'currency',
				currency : 'COP',
				maximumFractionDigits: 0,
				minimunFractionDigits : 0
			}).format(n);
	}

	quitarFormatoMoneda(moneda){

			moneda = moneda.replace("$","");
			moneda = moneda.replaceAll(".","");
			moneda = parseFloat(moneda);
			return(moneda); //returns --> 1200.51

	}

	sumandoArray(array){

		return array.reduce((a, b) => a + b, 0);
	}

	mostrarEstimadoFinanciado(financiados){

		if (financiados.estado_respuesta == 1) {
				$btn_generar_pdf.removeAttribute('disabled');
				let listado = '';
				let datos = financiados.data;


				//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
				let array_valor_lote = [];
				let array_valor_pagar = [];
				let array_valor_interes =[];
				datos.forEach(estado =>{
						let tipo_lote = (estado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';

						let valor_pagar = this.quitarFormatoMoneda(estado.valor_pagar);

						let valor_lote = this.quitarFormatoMoneda(estado.valor);

						let interes = valor_pagar - valor_lote;

						//AGREGANDO TODOS LOS VALORES A UN ARRAY
						array_valor_lote.push(valor_lote);
						array_valor_pagar.push(valor_pagar);
						array_valor_interes.push(interes);

						listado +=`
						<tr>
							<td>${estado.numero_lote}</td>
							<td>${tipo_lote}</td>
							<td>${estado.nombre}</td>
							<td>${estado.valor}</td>
							<td>${estado.numero_cuotas}</td>
							<td>${estado.valor_pagar}</td>
							<td>${this.formatoNumeroMiles(interes)}</td>
						</tr>`;
				});

				$cargar_estimado_financiado.innerHTML = listado;

				let suma_valor_lotes = this.sumandoArray(array_valor_lote);
				let suma_valor_ingreso = this.sumandoArray(array_valor_pagar);
				let suma_valor_interes = this.sumandoArray(array_valor_interes);

				$total_lotes.textContent = this.formatoNumeroMiles(suma_valor_lotes);
				$total_ingreso.textContent = this.formatoNumeroMiles(suma_valor_ingreso);
				$total_interes.textContent = this.formatoNumeroMiles(suma_valor_interes);
		} else{
				$error_mostrar.style.display = 'block';
				$btn_generar_pdf.setAttribute('disabled', 'disabled');
		}
	}

	generarPdfEstimado(estimados){

		let doc = new jspdf.jsPDF()
		doc.addImage({
				imageData : `${url_javascript}public/img/logo-puertas-del-sol.jpg`,
				x         : 160,
				y         : 10,
				w         : 25,
				h         : 29
		});

		doc.setFont("courier");
		doc.setFontSize(12);
		let fecha = new Date();
		let options = {year: 'numeric', month: 'long', day: 'numeric' };
		let array = [];
		//ARRAY VACIOS PARA SUMAR LAS CANTIDADES POR UN ARRAY
		let array_valor_lote = [];
		let array_valor_pagar = [];
		let array_valor_interes =[];
		let array_totales = [];

		doc.setFont('Courier', 'bold');
		doc.text(`Jamundi Valle,`, 10, 60);
		doc.setFont("courier", 'normal');
		doc.text(`${fecha.toLocaleDateString("es-ES", options)}`, 48, 60)
		doc.setFontSize(12);
		doc.setFont('Courier', 'bold');
		doc.text('ESTIMADO DE VENTAS POR ESTADO FINANCIADO', 100, 80, {align: 'center'});

		estimados.data.forEach(estimado =>{

				let tipo_lote = (estimado.id_tipo_lote == 1) ? 'Vivienda' : 'Comercial';
				let valor_pagar = this.quitarFormatoMoneda(estimado.valor_pagar);
				let valor_lote = this.quitarFormatoMoneda(estimado.valor);
				let interes = valor_pagar - valor_lote;

				//AGREGANDO TODOS LOS VALORES A UN ARRAY
				array_valor_lote.push(valor_lote);
				array_valor_pagar.push(valor_pagar);
				array_valor_interes.push(interes);

				array.push([estimado.numero_lote, tipo_lote, estimado.nombre, estimado.valor, estimado.numero_cuotas, estimado.valor_pagar, this.formatoNumeroMiles(interes)]);

		});

		let suma_valor_lotes = this.sumandoArray(array_valor_lote);
		let suma_valor_ingreso = this.sumandoArray(array_valor_pagar);
		let suma_valor_interes = this.sumandoArray(array_valor_interes);

		array_totales.push([this.formatoNumeroMiles(suma_valor_lotes), this.formatoNumeroMiles(suma_valor_ingreso), this.formatoNumeroMiles(suma_valor_interes)]);


		let head_total = [['Total Valor Lotes', 'Total Ingreso Cons.', 'Total Intereses']];
		let body_total = array_totales;
		doc.autoTable({
			head: head_total,
			body: body_total,
			startY: 95,
			theme : 'grid',
			styles:{
					halign: 'center'
			}
		});

		let head = [['# Lote', 'Tipo Lote', 'Cliente','Valor Lote', 'Cuotas', 'Valor Ingreso Cons.', 'Interes']]
		let body = array;
		doc.autoTable({
			head: head,
			body: body ,
			startY: 120,
			styles:{
				halign: 'center'
			}
		});
		window.open(doc.output('bloburl'), '_blank');
	}
}

const financiado = new Financiado();

document.addEventListener('DOMContentLoaded', e =>{

	financiado.cargarEstimadoFinanciado()
	.then(estado =>{
			financiado.mostrarEstimadoFinanciado(estado);
	});

});

$btn_generar_pdf.addEventListener('click', e =>{

	financiado.cargarEstimadoFinanciado()
	.then(resp =>{
			financiado.generarPdfEstimado(resp);
	});
});
