import {Ajax} from './helpers/ajax.js';

const $lotes_por_estado = document.getElementById('lotes-por-estado').getContext('2d');
const $progress_wrapper = document.querySelector('.progress-wrapper');
const $alerta_mora = document.querySelector('#alerta_mora');

const ajax = new Ajax();

class Index{

		async cargarLotesPorEstado(){

			try{
					let peticion = await ajax.fetchData(`AjaxRequest/cargar_cantidad_lotes_por_estado/`);
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
		}

		async cargarTotalLotesVendidos(){

			try{
					let peticion = await ajax.fetchData(`AjaxRequest/cargar_total_lotes_y_vendidos/`);
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
		}

		async cargarUltimoPagosLotes(){

			try{
				let peticion = await ajax.fetchData(`AjaxRequest/cargar_ultimo_pago_lotes`);
				return peticion;
			} catch(e){
				console.log('ERROR => ', e);
			}
		}

		mostrarGraficaLotesEstado(ctx, estado_lotes){

			if (estado_lotes.estado_respuesta == 1) {
					let valor_estados = [];
					let nombre_estados = [];
					let datos = estado_lotes.data;

					valor_estados.push(datos.total_disponible, datos.total_abonado, datos.total_separado, datos.total_contado, datos.total_lotes);

					let colores = [
					'hsl(141, 71%, 48%)',
					'hsl(48, 100%, 67%)',
					'hsl(204, 86%, 53%)',
					'hsl(348, 100%, 61%)',
					'hsl(171, 100%, 41%)'];

					let n_colores =[];

						for (let i = 0; i < valor_estados.length; i++) {
								n_colores.push(colores[i]);
						}

						//console.log(n_colores);

						let myChart = new Chart(ctx, {
							type: 'doughnut',
							data: {
								labels: ['Disponibles', 'Financiados', 'Separados', 'Contado', 'Total lotes'],
								datasets: [{
									data: valor_estados,
									backgroundColor: n_colores,
									borderWidth: 1
								}]
							}
						});
			}

		}

		mostrarPorcentajeLotes(data){
			let total_lotes = parseInt(data[0].total_lotes);
			let vendidos = parseInt(data[0].total_vendidos);
			let porcentaje = (vendidos / total_lotes) * 100;

			$progress_wrapper.innerHTML = `<progress class="progress is-danger is-large" value="${porcentaje}" max="100">${porcentaje}%</progress>
				<p class="progress-value has-text-white">${porcentaje}%</p>`;

		}
}

const grafica = new Index();


document.addEventListener('DOMContentLoaded', e =>{

		grafica.cargarLotesPorEstado()
			.then(lotes => {
					grafica.mostrarGraficaLotesEstado($lotes_por_estado, lotes);
		});

		grafica.cargarTotalLotesVendidos()
		.then(total =>{
			grafica.mostrarPorcentajeLotes(total.data);
		});

		grafica.cargarUltimoPagosLotes()
		.then(lotes => {

			let lista = '';

			lotes.data.forEach((sn) =>{

				lista +=`
				<article class="message is-danger is-small">
					<div class="message-body">
						Señor(a) <strong>${sn.nombres} ${sn.apellidos}</strong>, con lote numero ${sn.numero_lote}, de tipo ${sn.tipo_lote} se encuentra en mora, su numero de contacto es <strong>${sn.celular}</strong> ultima fecha de abono <strong>${sn.ultimo_pago}</strong><a href="${url_javascript}comprador/abona_lote/${sn.id_lote}"> Clic para ir a la hoja del lote</a>
					</div>
				</article>`;
			});

			$alerta_mora.innerHTML = lista;

		})

});


