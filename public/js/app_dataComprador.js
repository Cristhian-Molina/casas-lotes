import {Ajax} from './helpers/ajax.js';

const $terminar_proceso = document.querySelector('#terminar_proceso');
const $tipo_lote_valor = document.querySelector('#tipo_lote_valor');
const $numero_lote_valor = document.querySelector('#numero_lote_valor');
const $valor_lote = document.querySelector('#valor_lote');
const $tipo_pago = document.querySelector('#tipo_pago');
const $mostrar_bloque_contado = document.querySelector('.mostrar_bloque_contado');
const $mostrar_bloque_financiado = document.querySelector('.mostrar_bloque_financiado');
const $aplicar_descuento = document.querySelector('#aplicar_descuento');
const $total_lote_descuento = document.querySelector('#total_lote_descuento');
const $valor_descontado = document.querySelector('#valor_descontado');
const $observaciones = document.querySelector('#observaciones');
const $form_agregar_lote_comprador = document.querySelector('#form_agregar_lote_comprador');
const $valor_interes = document.querySelector('#valor_interes');
const $lote_cuotas = document.querySelector('#lote_cuotas');


const ajax = new Ajax();

class DataComprador{

		async eliminarComprador(id_comprador){

				try{
						let peticion = await ajax.fetchData(`AjaxRequest/eliminar_comprador/${id_comprador}`);
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async cargarTodosLotesSeleccionado(id){

				try{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_todos_lotes_seleccionado/${id}`);
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async capturarValoresLoteSeleccionados(data){

				try{
						let peticion = await ajax.fetchData(`AjaxRequest/cargar_valor_lote_seleccionado/`, data, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async guardarDatosLote(form){

				try{
						let peticion = await ajax.fetchData('AjaxRequest/agregar_datos_lote/', form, 'POST');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		async cargarTodosAsesores(){

				try{
						let peticion = await ajax.fetchData('AjaxRequest/cargar_todos_vendedores/');
						return peticion;
				} catch(e){
					console.log('ERROR =>', e);
				}
		}

		showPikaday(element){

				var picker = new Pikaday({
						field: document.getElementById(element),
						format: 'YYYY-MM/DD',
						i18n: {
								previousMonth : 'Anterior',
								nextMonth     : 'Siguiente',
								months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
								weekdays      : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
								weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
						},
						toString(date, format) {
									// you should do formatting based on the passed format,
									// but we will just return 'D/M/YYYY' for simplicity
									let day = date.getDate();
									let month = date.getMonth() + 1;
									let year = date.getFullYear();
										if(day < 10){
												day = '0'+ day; //agrega cero si el menor de 10
										}
										if(month < 10){
												month = '0' + month //agrega cero si el menor de 10
										}
									return `${year}-${month}-${day}`;
						},
						parse(dateString, format) {
								// dateString is the result of `toString` method
								const parts = dateString.split('-');
								const day = parseInt(parts[0], 10);
								const month = parseInt(parts[1], 10) - 1;
								const year = parseInt(parts[2], 10);
								return new Date(year, month, day);
						}
				});

				return picker;
		}


		formatoNumeroMiles(n){

				return new Intl.NumberFormat('es-CO', {
					style : 'currency',
					currency : 'COP',
					maximumFractionDigits: 0,
					minimumFractionDigits: 0
				}).format(n);
		}

		quitarFormatoMoneda(moneda){

				moneda = moneda.replace("$","");
				moneda = moneda.replaceAll(".","");
				moneda = parseFloat(moneda);
				return(moneda); //returns --> 1200.51

		}

		validarDatosLote(){

				let form = $form_agregar_lote_comprador;
				let selec_tipo_lote   = form.tipo_lote_valor.selectedIndex;
				let selec_numero_lote = form.numero_lote_valor.selectedIndex;
				let selec_forma_pago  = form.tipo_pago.selectedIndex;
				let selec_asesores    = form.cargar_asesores.selectedIndex;
				let fecha_abono       = form.fecha_abono;

				if(selec_tipo_lote == null || selec_tipo_lote == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste el tipo del lote!'
						});
						return false;
				} else if(selec_numero_lote == null || selec_numero_lote == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste un numero de lote!'
						});
						return false;
				} else if(fecha_abono.value == ''){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste una fecha!'
						});
						return false;
				} else if(selec_forma_pago == null || selec_forma_pago == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste un metodo de pago para el lote!'
						});
						return false;
				} else if($aplicar_descuento == ''){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer el valor al descuento del lote esta vacio!'
						});
						return false;
				} else if(selec_asesores == null || selec_asesores == 0){
						Swal.fire({
								type: 'error',
								title: 'Opps..',
								text: 'Al parecer no seleccionaste un asesor!'
						});
						return false;
				}

				return true;
		}

		mostrarAsesores(asesores){

			const $error_select_asesores = document.querySelector('.eror-data');
			const $cargar_asesores = document.querySelector('#cargar_asesores');
			if (asesores.estado_respuesta == 0) {
				$error_select_asesores.style.display ='block';
			}else{
					let listado = '';
					listado +='<option value="">Seleccione..</option>';
					let datos = asesores.data;
					datos.forEach(asesor =>{
							listado +=
							`<option value="${asesor.id}">${asesor.nombres} ${asesor.apellidos}</option>`;
					});
					$cargar_asesores.innerHTML = listado;
			}

		}

		mostarNumeroCuotas(){
				const $lote_cuotas = $form_agregar_lote_comprador.lote_cuotas;
				let listado = '';
				listado +='<option value="">Seleccione...</option>';

				for (let i=1; i <=60; i++) {
						listado += `<option value="${i}">${i}</option>`;
				}

				$lote_cuotas.innerHTML = listado;
		}


}


//INSTANCIA
const dataComprador = new DataComprador();

document.addEventListener('DOMContentLoaded', () =>{

		dataComprador.showPikaday('fecha_abono');
		dataComprador.cargarTodosAsesores()
		.then(asesores =>{
				dataComprador.mostrarAsesores(asesores);
		});

		dataComprador.mostarNumeroCuotas();

});

//LISTENER

$form_agregar_lote_comprador.addEventListener('submit', e=>{

		e.preventDefault();
		if (dataComprador.validarDatosLote()) {

				let plantilla_contado = `(A este lote se le aplicó descuento del ${$aplicar_descuento.value}% equivalente a ${$valor_descontado.value} y el valor pagado por el comprador es de ${$total_lote_descuento.value})`;

				
				let informacion ={
					'mensaje_escrito' : $observaciones.value,
					'mesaje_plantilla' : plantilla_contado
				};

				let form = new FormData(e.target);
				if ($tipo_pago.value == 1) {
						form.append('observaciones', Object.values(informacion));
						dataComprador.guardarDatosLote(form)
						.then(data => {
								window.location.href = `${url_javascript}dashboard/lotes/`;
						});
				}
				else{
						dataComprador.guardarDatosLote(form)
						.then(data =>{
								window.location.href = `${url_javascript}dashboard/lotes/`;
						});
				}
		}

});


$terminar_proceso.addEventListener('click', e =>{

	Swal.fire({
			title: 'Estas seguro en terminar el proceso?',
			text: "Esta acción eliminara los datos ya guardados del reciente comprador!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, terminalo!'
	})
	.then(result =>{
			if (result.value) {

					dataComprador.eliminarComprador($terminar_proceso.dataset.id)
					.then(eliminar_comprador =>{
								console.log(eliminar_comprador);
							if (eliminar_comprador.estado_respuesta == 0) {
									Swal.fire({
											type: 'error',
											title: 'Oops...',
											text: 'Al parecer no se pudo eliminar el comprador'
									});
									//return false;
							}
							window.location.href = `${url_javascript}dashboard/comprador/`;
					})
			}
	})
});


$tipo_lote_valor.addEventListener('change', e =>{

		dataComprador.cargarTodosLotesSeleccionado(e.target.value)
		.then(lotes_cargados => {
				if (lotes_cargados.estado_respuesta == 0) {
						Swal.fire({
							type: 'error',
							title: 'Oops...',
							text: 'No hay información que mostrar!'
						})
						.then(() =>{
								location.reload();
						});
				} else{
						$numero_lote_valor.length = 0;
						let mostrar_lotes = lotes_cargados.data;
						$numero_lote_valor.innerHTML = `<option value="0">Seleccione..</option>`
						mostrar_lotes.forEach(element =>{
								$numero_lote_valor.innerHTML +=
								`<option value="${element.numero_lote}">${element.numero_lote}</option>`;
						});
				}
		});
});


$numero_lote_valor.addEventListener('change', e =>{

		let form = new FormData();
		form.append('tipo_lote', $tipo_lote_valor.value);
		form.append('numero_lote', e.target.value);
		dataComprador.capturarValoresLoteSeleccionados(form)
		.then(valor_lote => {
				$valor_lote.value = valor_lote.data[0].valor;

		});
});


$tipo_pago.addEventListener('change', e =>{

		if (e.target.value == 1) {
				$mostrar_bloque_contado.style.display = 'block';
				$mostrar_bloque_financiado.style.display = 'none';
				$valor_interes.value ="";
				$form_agregar_lote_comprador.abono.value = "";
				$lote_cuotas.value = "";
				$form_agregar_lote_comprador.total_deuda.value = "";
				$form_agregar_lote_comprador.valor_por_cuota.value = "";
				$form_agregar_lote_comprador.total_lote_financiado.value = "";

				//DESABILITAR LOS CAMPOS DEL BLOQUE FINANCIADO
				$form_agregar_lote_comprador.valor_interes.disabled = true;
				$form_agregar_lote_comprador.abono.disabled = true;
				$form_agregar_lote_comprador.lote_cuotas.disabled = true;
				$form_agregar_lote_comprador.total_deuda.disabled = true;
				$form_agregar_lote_comprador.valor_por_cuota.disabled = true;
				$form_agregar_lote_comprador.total_lote_financiado.disabled = true;

		}else if(e.target.value == 2){
				$mostrar_bloque_financiado.style.display = 'block';
				$mostrar_bloque_contado.style.display = 'none';
				$aplicar_descuento.value = "";
				$total_lote_descuento.value = "";
				$valor_descontado.value = "";
				$observaciones.value = "";

				$form_agregar_lote_comprador.valor_interes.disabled = false;
				$form_agregar_lote_comprador.abono.disabled = false;
				$form_agregar_lote_comprador.lote_cuotas.disabled = false;
				$form_agregar_lote_comprador.total_deuda.disabled = false;
				$form_agregar_lote_comprador.valor_por_cuota.disabled = false;
				$form_agregar_lote_comprador.total_lote_financiado.disabled = false;
		}else if(e.target.value == 3){
				$mostrar_bloque_financiado.style.display = 'none';
				$mostrar_bloque_contado.style.display = 'none';
				document.querySelector('.medio_pago').style.display = 'none';

		}else if(e.target.value == 0){
			$mostrar_bloque_financiado.style.display = 'none';
			$mostrar_bloque_contado.style.display = 'none';
		}
});


$aplicar_descuento.addEventListener('change', e =>{

		let valor_porcentaje = e.target.value;
		let valor_lote_formateado = dataComprador.quitarFormatoMoneda($valor_lote.value);

		let resultado_descuento = (valor_lote_formateado * valor_porcentaje) / 100;
		$valor_descontado.value = dataComprador.formatoNumeroMiles(resultado_descuento);
		let valor_lote_descuento = (valor_lote_formateado - resultado_descuento);
		$total_lote_descuento.value = dataComprador.formatoNumeroMiles(valor_lote_descuento);
});



$form_agregar_lote_comprador.abono.addEventListener('change', e => {

		const element = e.target;
		const value = element.value;
		element.value = dataComprador.formatoNumeroMiles(value);
		const recal = dataComprador.quitarFormatoMoneda($valor_lote.value) - dataComprador.quitarFormatoMoneda(value);
		//console.log(recal);
		$form_agregar_lote_comprador.total_deuda.value = dataComprador.formatoNumeroMiles(recal)
});


$valor_interes.addEventListener('change', e =>{
		let interes = parseInt(e.target.value);
		//let valor_lote_formateado = parseInt($valor_lote.value.replaceAll('.', ''));
		let valor_lote_formateado = dataComprador.quitarFormatoMoneda($valor_lote.value);
		//let prueba= quitarFormatoMoneda(valor_lote_formateado);
		let valor_porcentaje = interes / 100;

		//CALCULAR EL VALOR DE LA CUOTA INICIAL 
		let cuota_inicial = (valor_lote_formateado * valor_porcentaje);
		$form_agregar_lote_comprador.abono.value = dataComprador.formatoNumeroMiles(cuota_inicial);
		//let abono = parseInt($form_agregar_lote_comprador.abono.value.replaceAll('.', ''));
		let abono = dataComprador.quitarFormatoMoneda($form_agregar_lote_comprador.abono.value);

		let total_deuda = valor_lote_formateado - abono;
		//REEMPLAZAR EL VALOR DEL CAMPO ABONO POR EL 
		$form_agregar_lote_comprador.total_deuda.value = dataComprador.formatoNumeroMiles(total_deuda);


});



$lote_cuotas.addEventListener('change', e =>{

		const form = $form_agregar_lote_comprador;
		let cuota = parseInt(e.target.value);
		let total_deuda = dataComprador.quitarFormatoMoneda(form.total_deuda.value);


		let interes = form.valor_interes.value / 100;
		let valor_por_cuota = parseInt(form.valor_por_cuota.value);

		if (cuota <= 12) {

				//let sin_interes = total_deuda / 12;
				let sin_interes = total_deuda / cuota;
				form.valor_por_cuota.value = dataComprador.formatoNumeroMiles(sin_interes);

		} else {
				
				let total_cuota_a_mes = total_deuda /( ( 1- (1 + 0.01) ** (-cuota)) / 0.01);

				//form.valor_por_cuota.value = dataComprador.formatoNumeroMiles(Math.round(total_cuota_a_mes));
				form.valor_por_cuota.value = dataComprador.formatoNumeroMiles(total_cuota_a_mes);
		}

		let abono_formateado = dataComprador.quitarFormatoMoneda(form.abono.value);
		let valor_por_cuota_formateada = dataComprador.quitarFormatoMoneda(form.valor_por_cuota.value);

		let valor_total_financiado = (valor_por_cuota_formateada * cuota) + abono_formateado;

		form.total_lote_financiado.value = dataComprador.formatoNumeroMiles(valor_total_financiado);

})
