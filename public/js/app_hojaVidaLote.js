import {Ajax} from './helpers/ajax.js';

const $cargar_abonos_id_lote    = document.querySelector('#cargar_abonos_id_lote');
const $hidden_id_lote = document.querySelector('#hidden_id_lote');

const ajax = new Ajax();

class HojaVidaLote {

	formatoNumeroMiles(n){
			return new Intl.NumberFormat('es-CO', {
				style : 'currency',
				currency : 'COP',
				maximumFractionDigits: 0,
				minimunFractionDigits : 0
			}).format(n);
	}

	quitarFormatoMoneda(moneda){

			moneda = moneda.replace("$","");
			moneda = moneda.replaceAll(".","");
			moneda = parseFloat(moneda);
			return(moneda); //returns --> 1200.51

	}


	async cargarDetalleAbonos(){

			try{
					let peticion = await ajax.fetchData(`AjaxRequest/cargar_detalles_abonos/${hidden_id_lote.value}`);
					return peticion;
			} catch(e){
				console.log('ERROR =>', e);
			}
	}


	mostrarDetalleAbonos(obj, total_lote){

			let listado = '';
			let tipo_abono = '';
			let total = this.quitarFormatoMoneda(total_lote);
			obj.forEach(abono =>{

					if (abono.forma_abono_comprador == 1) {
							tipo_abono = 'Efectivo';
					} else if(abono.forma_abono_comprador == 2){
							tipo_abono = 'Bancolombia - #2175';
					} else if(abono.forma_abono_comprador == 3){
							tipo_abono = 'Bancolombia - #1063'
					} else if(abono.forma_abono_comprador == 4){
							tipo_abono = 'Banco Occidente - #6391'
					} else if(abono.forma_abono_comprador == 5){
							tipo_abono = 'Banco Occidente - #6383'
					}
					total = (total - this.quitarFormatoMoneda(abono.valor_abono));

					listado += `
					<tr>
						<td>${abono.fecha}</td>
						<td>${abono.concepto}</td>
						<td>${abono.numero_recibo}</td>
						<td>${tipo_abono}</td>
						<td>${abono.observaciones}</td>
						<td>${abono.valor_abono}</td>
						<td>${this.formatoNumeroMiles(total)}</td>`;
			});
			$cargar_abonos_id_lote.innerHTML = listado;
	}
}


const vidaLote = new HojaVidaLote();

document.addEventListener('DOMContentLoaded', e =>{

	vidaLote.cargarDetalleAbonos()
	.then(abonos =>{
			if (abonos.estado_respuesta == 1) {
				vidaLote.mostrarDetalleAbonos(abonos.data, valor_lote.value)
			}

	})

});