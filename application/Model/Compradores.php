<?php

namespace Casas_lotes\Model;

use Casas_lotes\Core\Model;
use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';

Class Compradores extends Model
{

	public function agregar_nuevo_comprador($data)
	{
			$sql = "INSERT INTO compradores (nombres, apellidos, id_identificacion, numero_identificacion, direccion_residencial, correo_electronico, celular, dia_pago)
			VALUES (:nombres, :apellidos, :id_identificacion, :numero_identificacion, :direccion_residencial, :correo_electronico, :celular, :dia_pago)";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':nombres' => $data['nombres'],
													':apellidos' => $data['apellidos'],
													':id_identificacion' =>$data['tipo_documento'],
													':numero_identificacion' =>$data['numero_identificacion'],
													':direccion_residencial' =>$data['direccion'],
													':correo_electronico' =>$data['correo_electronico'],
													':celular' => $data['celular'],
													':dia_pago' => $data['dia_pago']);

			echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
				$stmt->execute($parametros);

					if ($stmt->rowCount() == 1) {
							$resultado = array('estado_respuesta' => 1, 'id_comprador'  => $this->db->lastInsertid());
							return $resultado;
					}else{
							return array('estado_respuesta' => 0);
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function mostrar_un_comprador($id)
	{
			$sql = "SELECT * FROM compradores WHERE id = :id_comprador";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':id_comprador' => $id);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
					$stmt->execute($parametros);
					return $stmt->fetch();
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function eliminar_un_comprador($id)
	{
			$sql = "DELETE FROM compradores WHERE id = :id_comprador";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':id_comprador' => $id);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();

			try {
					$stmt->execute($parametros);

					if ($stmt->rowCount() == 1) {
							return true;
					} else{
							return false;
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function buscar_comprador_cedula($cedula)
	{
			$sql = "SELECT * FROM compradores WHERE numero_identificacion =:numero_identificacion";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':numero_identificacion' => $cedula);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();

			try {
					$stmt->execute($parametros);
					return $stmt->fetch();
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function buscar_comprador_id($id)
	{
			$sql = "SELECT * FROM compradores WHERE id =:id_comprador";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':id_comprador' => $id);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();

			try {
					$stmt->execute($parametros);
					return $stmt->fetch();
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function actualizar_un_comprador($data)
	{
			$sql = "UPDATE compradores SET 
							nombres =:nombres_update,
							apellidos =:apellidos_update,
							id_identificacion =:id_documento_update,
							numero_identificacion =:numero_identificacion,
							direccion_residencial =:direccion_residencial,
							correo_electronico =:correo_electronico,
							celular =:celular,
							dia_pago = :dia_pago
							WHERE id = :id_comprador";
			
			$parametros = array(':nombres_update' => $data['nombres_update'],
													':apellidos_update' => $data['apellidos_update'],
													':id_documento_update' => $data['tipo_documento_update'],
													':numero_identificacion' => $data['numero_identificacion_update'],
													':direccion_residencial' => $data['direccion_update'],
													':correo_electronico' => $data['correo_update'],
													':celular' => $data['celular_update'],
													'dia_pago' => $data['dia_pago_update'],
													':id_comprador' => $data['id_comprador_update']);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
					$stmt = $this->db->prepare($sql);
					$stmt->execute($parametros);

					if($stmt->rowCount() == 1){
							return true;
					} else{
							return false;
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}

	public function buscar_comprador_lote($data)
	{
			$sql = "SELECT * FROM compradores WHERE id = 
								(SELECT id_comprador FROM abonos WHERE id_lote =
									(SELECT id FROM lotes WHERE id_tipo_lote = :tipo_lote AND numero_lote = :numero_lote)
								GROUP BY id_lote)";

			$stmt = $this->db->prepare($sql);
			$parametros = array(':tipo_lote' => $data['buscar_tipo_lote'], ':numero_lote' => $data['buscar_lote']);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
					$stmt->execute($parametros);
					return $stmt->fetch();
			} catch (\Exception $e) {
					return $e->getCode();
			}

	}

	public function mostar_comprador_nombre($data)
	{
			$sql = "SELECT id, nombres, apellidos FROM compradores WHERE nombres like :nombres";
			$stmt = $this->db->prepare($sql);
			$parametros = array(':nombres' => '%'.$data['buscar_nombre'].'%');
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
					$stmt->execute($parametros);
					return $stmt->fetchAll();
			} catch (\Exception $e) {
					return $e->getCode();
			}
	}


}