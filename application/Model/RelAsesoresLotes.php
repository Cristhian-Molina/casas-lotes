<?php

namespace Casas_lotes\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

use Casas_lotes\Core\Model;

Class RelAsesoresLotes extends Model
{

		public function ingresar_asesor_lote($asesor, $lote)
		{
				$sql = "INSERT INTO rel_asesor_lote (id_asesor, id_lote) VALUES(:id_asesor, :id_lote)";

				$stmt = $this->db->prepare($sql);
				$parametros = array(':id_asesor' => $asesor,
														':id_lote' => $lote);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
					$stmt->execute($parametros);

						if ($stmt->rowCount() == 1) {
								return array('estado_respuesta' => 1);
						}else{
								return array('estado_respuesta' => 0);
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function eliminar_asesores_lote_id($id_lote)
		{

				$sql = "DELETE FROM rel_asesor_lote WHERE id_lote = :id_lote";
				$stmt = $this->db->prepare($sql);
				$parametros = array(':id_lote' => $id_lote);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
					$stmt->execute($parametros);

						if ($stmt->rowCount() == 1) {
								return true;
						}else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function consultar_cantidad_lotes_asesor()
		{

				$sql = "SELECT
								rel.id_asesor as id_asesor,
								a.nombres,
								COUNT(id_asesor) as total_lotes
								FROM rel_asesor_lote as rel
								INNER JOIN asesores as a ON a.id = rel.id_asesor
								GROUP BY id_asesor";

				$stmt = $this->db->prepare($sql);
				try {
						$stmt->execute();
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}
}