<?php

namespace Casas_lotes\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

use Casas_lotes\Core\Model;

Class Desistimientos extends Model
{

		public function insertar_data_desistimiento($id_lote, $id_comprador, $suma_abonos)
		{

				$sql = "INSERT INTO desistimientos (id_lote, id_comprador, total_abonado, fecha, id_usuario) VALUES(:id_lote, :id_comprador, :total_abonado, :fecha, :id_usuario)";

				$stmt = $this->db->prepare($sql);
				$parametros = array(':id_lote' =>$id_lote,
														':id_comprador' => $id_comprador,
														':total_abonado' => $suma_abonos,
														':fecha' => date('Y-m-d'),
														':id_usuario' => $_SESSION['sesion_usuario']);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();

				try {
						$stmt->execute($parametros);

						if ($stmt->rowCount() == 1) {
								return array('estado_respuesta' => 1);
						}else{
							return array('estado_respuesta' => 0);
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function consultar_data_desistimientos_data_comprador()
		{

				$sql = "SELECT
								c.nombres,
								c.apellidos,
								c.numero_identificacion,
								c.celular,
								l.id_tipo_lote,
								l.numero_lote,
								d.total_abonado,
								d.fecha
								FROM desistimientos AS d
								INNER JOIN compradores AS c
								ON c.id = d.id_comprador
								INNER JOIN lotes AS l
								ON l.id = d.id_lote";

				$stmt = $this->db->prepare($sql);
				try {
						$stmt->execute();
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}
}