<?php

namespace Casas_lotes\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

use Casas_lotes\Core\Model;

Class Lotes extends Model
{

		public function mostrar_lotes_vivienda()
		{
			$sql = "SELECT * FROM lotes WHERE id_tipo_lote = :id ORDER BY numero_lote DESC";
			$parametros = array(':id' => 1);
			try {
					$stmt = $this->db->prepare($sql);
					$stmt->execute($parametros);
					return $stmt->fetchAll();

			} catch (\Exception $e) {
					return $e->getCode();
			}

		}

		public function mostrar_data_un_lote($id, $tipo_lote)
		{
				$sql = "SELECT * FROM lotes WHERE id_tipo_lote = :tipo_lote AND id = :id";
				$parametros = array(':tipo_lote' => $tipo_lote, ':id' => $id);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt = $this->db->prepare($sql);
						$stmt->execute($parametros);
						return $stmt->fetch();
				} catch (\Exception $e) {
						return $e->getCode();
				}

		}

		public function actualizar_un_lote_data($data)
		{
			$sql = "UPDATE lotes SET
							medidas =:medidas,
							valor =:valor
							WHERE id_tipo_lote = :tipo_lote AND numero_lote = :numero_lote";

			$parametros = array(':tipo_lote' => $data['valor_tipo_lote'],
													':numero_lote' => $data['numero_lote'],
													':medidas' => $data['medidas_update'],
													':valor' => $data['valor_update']);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			$stmt = $this->db->prepare($sql);
			
			try {
					$stmt->execute($parametros);

					if($stmt->rowCount() == 1){
							return true;
					} else{
							return false;
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}

		}

		public function actualizar_estado_lote($id, $estado)
		{

				$sql = "UPDATE lotes SET
								estado_lote =:estado
								WHERE id = :id";

				$parametros = array(':id' => $id,
														':estado' => $estado);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				$stmt = $this->db->prepare($sql);
				
				try {
						$stmt->execute($parametros);

						if($stmt->rowCount() == 1){
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function actualizar_estado_lote_data_separado($data, $estado)
		{

				$sql = "UPDATE lotes SET
								estado_lote =:estado,
								tipo_de_pago =:tipo_de_pago,
								valor_pagar =:valor_pagar,
								numero_cuotas =:numero_cuotas,
								cuota =:cuota
								WHERE id = :id";

				$parametros = array(':id' => $data['id_lote'],
														':tipo_de_pago' =>2,
														':valor_pagar' => $data['total_lote_financiado'],
														':numero_cuotas' => $data['lote_cuotas'],
														':cuota' => $data['valor_por_cuota'],
														':estado' => $estado);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				$stmt = $this->db->prepare($sql);
				
				try {
						$stmt->execute($parametros);

						if($stmt->rowCount() == 1){
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function mostrar_lotes_comercial()
		{
			$sql = "SELECT * FROM lotes WHERE id_tipo_lote = :id ORDER BY numero_lote DESC";
			$parametros = array(':id' => 2);
			try {
					$stmt = $this->db->prepare($sql);
					$stmt->execute($parametros);
					return $stmt->fetchAll();

			} catch (\Exception $e) {
					return $e->getCode();
			}

		}


		public function agregar_nuevo_lote($data)
		{
			$sql = "INSERT INTO lotes (id_tipo_lote, numero_lote, medidas, valor, estado_lote, tipo_de_pago, valor_pagar, numero_cuotas, cuota, id_usuario)
				VALUES (:tipo_lote, :numero_lote, :medidas, :valor, :estado_lote, :tipo_de_pago, :valor_pagar, :numero_cuotas, :cuota, :id_usuario)";

			$stmt = $this->db->prepare($sql);
			$parametros = array(
										':tipo_lote' => $data['tipo_lote'],
										':numero_lote' => $data['numero_lote'],
										':medidas' => $data['medidas_lote'],
										':valor' => $data['valor_lote'],
										':estado_lote' => 1,
										':tipo_de_pago' => 0,
										':valor_pagar' => 0,
										':numero_cuotas' => 0,
										':cuota' => 0,
										':id_usuario' => $_SESSION['sesion_usuario']
									);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
				$stmt->execute($parametros);

					if ($stmt->rowCount() == 1) {
							return array('estado_respuesta' => 1);
					}else{
							return array('estado_respuesta' => 0);
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}

		}

		public function mostrar_todos_lotes_seleccionado($id)
		{
				$sql = "SELECT numero_lote, valor FROM lotes WHERE id_tipo_lote = :id AND estado_lote = 1";
				$stmt = $this->db->prepare($sql);
				$parametros = array(':id' => $id);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function mostrar_valor_lote_seleccionado($data)
		{
				$sql = "SELECT valor FROM lotes WHERE id_tipo_lote = :id_tipo_lote AND numero_lote = :numero_lote AND estado_lote = 1";
				$stmt = $this->db->prepare($sql);
				$parametros = array(':id_tipo_lote' => $data['tipo_lote'],
														':numero_lote' => $data['numero_lote']);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}


		public function mostrar_id_lote_por_tipo_y_numero($tipo, $numero)
		{
				$sql = "SELECT id FROM lotes WHERE id_tipo_lote = :tipo AND numero_lote = :numero";
				$stmt = $this->db->prepare($sql);

				$parametros = array(':tipo' => $tipo,
														':numero' => $numero);

				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetch();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function actualizar_lote_despues_abono_contado($data, $id_lote)
		{
				$sql = "UPDATE lotes SET
								estado_lote = :estado_lote,
								tipo_de_pago = :tipo_de_pago,
								valor_pagar = :valor_pagar
								WHERE id = :id_lote";

				$parametros = array(':estado_lote' => 4,
														':tipo_de_pago' => $data['tipo_pago'],
														':valor_pagar' => $data['total_lote_descuento'],
														':id_lote' => $id_lote
													);

				$stmt = $this->db->prepare($sql);
				
				try {
						$stmt->execute($parametros);

						if($stmt->rowCount() == 1){
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function actualizar_lote_despues_abono_financiado_separado($data, $id_lote, $estado_lote)
		{
				$sql = "UPDATE lotes SET
								estado_lote = :estado_lote,
								tipo_de_pago = :tipo_de_pago,
								valor_pagar = :valor_pagar,
								numero_cuotas = :numero_cuotas,
								cuota = :cuota
								WHERE id = :id_lote";

				$parametros = array(':estado_lote' => $estado_lote,
														':tipo_de_pago' => $data['tipo_pago'],
														':valor_pagar' => $data['total_lote_financiado'],
														':numero_cuotas' => $data['lote_cuotas'],
														':cuota' => $data['valor_por_cuota'],
														':id_lote' => $id_lote
													);

				$stmt = $this->db->prepare($sql);
				
				try {
						$stmt->execute($parametros);

						if($stmt->rowCount() == 1){
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function consultar_total_lotes_y_vendidos()
		{

				$sql = "SELECT
								COUNT(id) total_lotes,
								(SELECT COUNT(estado_lote) FROM lotes WHERE estado_lote IN (2,4) ) as total_vendidos
								FROM lotes";

				$stmt = $this->db->prepare($sql);

				try {
						$stmt->execute();
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function consultar_total_lotes_por_estado()
		{
			$sql = "SELECT
							COUNT(id) total_lotes,
							(SELECT COUNT(estado_lote) FROM lotes WHERE estado_lote = 1) AS total_disponible,
							(SELECT COUNT(estado_lote) FROM lotes WHERE estado_lote = 2) AS total_abonado,
							(SELECT COUNT(estado_lote) FROM lotes WHERE estado_lote = 3) AS total_separado,
							(SELECT COUNT(estado_lote) FROM lotes WHERE estado_lote = 4) AS total_contado
							FROM lotes";

			$stmt = $this->db->prepare($sql);

			try {
					$stmt->execute();
					return $stmt->fetch();
			} catch (\Exception $e) {
					return $e->getCode();
			}
		}

		public function estimado_disponibles()
		{
			$sql = "SELECT 
							numero_lote,
							id_tipo_lote,
							valor
							FROM lotes
							WHERE estado_lote = 1";

			$stmt = $this->db->prepare($sql);

				try {
						$stmt->execute();
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}

		}
}