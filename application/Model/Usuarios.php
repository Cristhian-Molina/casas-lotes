<?php

namespace Casas_lotes\Model;

use Casas_lotes\Core\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

Class Usuarios extends Model
{

	public function validar_form_login_usuario($data)
	{
		$sql = "SELECT id, nombre, pass FROM usuarios WHERE nombre = :user";
		$smt = $this->db->prepare($sql);
		$parametros = array(':user' => $data['user']);

		try{

			$smt->execute($parametros);
			$resultado = $smt->fetch();

				if (empty($resultado)) {
						//NO EXISTE NINGUN USUARIO
						return array('estado_respuesta' => 0);
				//EXISTE EL USUARIO
				}else if($smt->rowCount() == 1){
							if (password_verify($data['password'], $resultado->pass)) {

									$resultado->estado_respuesta = 1;
									$resultado->estado_clave = 1;
									session_start();
									$_SESSION['sesion_usuario'] = $resultado->id;

									return $resultado;

							}else{
									return array('estado_clave' => 0);
							}
				}

		}catch(\Exception $e){
			return $e->getCode();
		}

	}//END FUNCTION VALIDAR_FORM_LOGIN_USUARIO

	public function obtener_todo_usuario($data)
	{
			$sql = "SELECT * FROM usuarios WHERE id =:id";
			$smt = $this->prepare($sql);
			$parametros = array(':id' => $data['id_usuario']);
	}
}