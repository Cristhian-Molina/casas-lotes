<?php

namespace Casas_lotes\Model;

use Casas_lotes\Core\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

Class Vendedores extends Model
{

		public function agregar_nuevo_vendedor_model($data)
		{
			$sql = "INSERT INTO asesores (nombres, apellidos, id_identificacion, numero_identificacion, celular, fecha_creado, id_usuario)
				VALUES (:nombre, :apellido, :id_identificacion, :numero_identificacion, :celular, :fecha_creado, :id_usuario)";

			$stmt = $this->db->prepare($sql);
			$parametros = array(
										':nombre' => $data['nombres'],
										':apellido' => $data['apellidos'],
										':id_identificacion' => $data['tipo_documento'],
										':numero_identificacion' => $data['numero_identificacion'],
										':celular' => $data['celular'],
										':fecha_creado' => date("Y-m-d"),
										':id_usuario' => $_SESSION['sesion_usuario']
									);
			//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
			try {
				$stmt->execute($parametros);

					if ($stmt->rowCount() == 1) {
							return array('estado_respuesta' => 1);
					}else{
							return array('estado_respuesta' => 0);
					}
			} catch (\Exception $e) {
					return $e->getCode();
			}

		}

		public function mostar_todos_vendedores()
		{
				$sql = "SELECT * FROM asesores";

				try {
						$stmt = $this->db->prepare($sql);
						$stmt->execute();
						return $stmt->fetchAll();

				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function mostar_un_vendedor($id)
		{
				$sql = "SELECT id, nombres, apellidos, id_identificacion, numero_identificacion, celular, id_usuario FROM asesores WHERE id = :id_vendedor";
				$parametros = array(':id_vendedor' => $id);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt = $this->db->prepare($sql);
						$stmt->execute($parametros);
						return $stmt->fetch();
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function actualizar_un_vendedor($data)
		{
				$sql = "UPDATE asesores SET 
								nombres =:nombres_update,
								apellidos =:apellidos_update,
								id_identificacion =:id_documento_update,
								numero_identificacion =:numero_identificacion,
								celular =:celular_update
								WHERE id = :id_vendedor";
				
				$parametros = array(':nombres_update' => $data['nombres_update'],
														':apellidos_update' => $data['apellidos_update'],
														':id_documento_update' => $data['tipo_documento_update'],
														':numero_identificacion' => $data['numero_identificacion_update'],
														':celular_update' => $data['celular_update'],
														':id_vendedor' => $data['id_vendedor_update']);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt = $this->db->prepare($sql);
						$stmt->execute($parametros);

						if($stmt->rowCount() == 1){
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}

		public function eliminar_un_vendedor($id)
		{
				$sql = "DELETE FROM asesores WHERE id =:id_vendedor";
				$parametros = array(':id_vendedor' => $id);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();

				$stmt = $this->db->prepare($sql);
				try {
						$stmt->execute($parametros);

						if ($stmt->rowCount() == 1) {
								return true;
						} else{
								return false;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}
		}
}