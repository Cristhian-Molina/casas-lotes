<?php

namespace Casas_lotes\Model;
/*use Casas_lotes\Libs\Helper;

include APP . 'Libs/helper.php';*/

use Casas_lotes\Core\Model;

Class Abonos extends Model
{
			public function insertar_nuevo_abono_contado($data, $id_lote)
			{
					$sql = "INSERT INTO abonos (id_lote, id_comprador, concepto, valor_abono, fecha, id_forma_pago, observaciones, numero_recibo, forma_abono_comprador, id_usuario)VALUES(
						:id_lote, :id_comprador, :concepto, :valor_abono, :fecha, :id_forma_pago, :observaciones, :numero_recibo, :forma_abono_comprador, :id_usuario)";

					$stmt = $this->db->prepare($sql);
					$parametros = array(
												':id_lote' => $id_lote,
												':id_comprador' => $data['id_comprador'],
												':concepto' => $data['concepto'],
												':valor_abono' => $data['total_lote_descuento'],
												':fecha' => $data['fecha_abono'],
												':id_forma_pago' => $data['tipo_pago'],
												':observaciones' => $data['observaciones'],
												':numero_recibo' => $data['numero_recibo'],
												':forma_abono_comprador' => $data['tipo_abono'],
												':id_usuario' => $_SESSION['sesion_usuario']
											);
					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
						$stmt->execute($parametros);

							if ($stmt->rowCount() == 1) {
									return array('estado_respuesta' => 1);
							}else{
									return array('estado_respuesta' => 0);
							}
					} catch (\Exception $e) {
							return $e->getCode();
					}

			}

			public function insertar_nuevo_abono_financiado_separado($data, $id_lote)
			{
					$sql = "INSERT INTO abonos (id_lote, id_comprador, concepto, valor_abono, fecha, id_forma_pago, observaciones, numero_recibo, forma_abono_comprador, id_usuario)VALUES(
						:id_lote, :id_comprador, :concepto, :valor_abono, :fecha, :id_forma_pago, :observaciones, :numero_recibo, :forma_abono_comprador, :id_usuario)";

					$stmt = $this->db->prepare($sql);
					$parametros = array(
												':id_lote' => $id_lote,
												':id_comprador' => $data['id_comprador'],
												':concepto' => $data['concepto'],
												':valor_abono' => $data['abono'],
												':fecha' => $data['fecha_abono'],
												':id_forma_pago' => $data['tipo_pago'],
												':observaciones' => $data['observaciones'],
												':numero_recibo' => $data['numero_recibo'],
												':forma_abono_comprador' => $data['tipo_abono'],
												'id_usuario' => $_SESSION['sesion_usuario']
											);
					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
						$stmt->execute($parametros);

							if ($stmt->rowCount() == 1) {
									return array('estado_respuesta' => 1);
							}else{
									return array('estado_respuesta' => 0);
							}
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function insertar_siguiente_abono_finandiado($data)
			{

					$sql = "INSERT INTO abonos (id_lote, id_comprador, concepto, valor_abono, fecha, id_forma_pago, observaciones, numero_recibo, forma_abono_comprador, id_usuario) VALUES (:id_lote, :id_comprador, :concepto, :valor_abono, :fecha, :id_forma_pago, :observaciones, :numero_recibo, :forma_abono_comprador, :id_usuario)";

					$stmt = $this->db->prepare($sql);

					$parametros = array(
												':id_lote' => $data['id_lote'],
												':id_comprador' => $data['id_comprador'],
												':concepto' => $data['concepto_abono'],
												':valor_abono' => $data['valor_abonado'],
												':fecha' => $data['fecha_abono'],
												':id_forma_pago' => 2,
												':observaciones' => $data['observaciones'],
												':numero_recibo' => $data['recibo_abono'],
												':forma_abono_comprador' => $data['tipo_abono'],
												':id_usuario' => $_SESSION['sesion_usuario']
											);

					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
						$stmt->execute($parametros);

							if ($stmt->rowCount() == 1) {
									return true;
							}else{
									return false;
							}
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function consultar_data_abono_comprador($id)
			{

					$sql = "SELECT DISTINCT
									l.id as id_lote,
									c.id as id_comprador,
									c.nombres,
									c.apellidos,
									c.numero_identificacion,
									c.direccion_residencial,
									c.correo_electronico,
									c.celular,
									l.id_tipo_lote,
									l.numero_lote,
									l.valor_pagar,
									l.cuota,
									l.numero_cuotas,
									MIN(a.id_forma_pago) AS id_forma_pago,
									(SELECT concat(nombres, ' ' ,apellidos) FROM asesores WHERE id = rel.id_asesor) as asesor
									FROM abonos AS a
									INNER JOIN lotes as l ON l.id = a.id_lote
									INNER JOIN compradores as c ON c.id = a.id_comprador
									INNER JOIN rel_asesor_lote as rel ON rel.id_lote = a.id_lote
									WHERE rel.id_lote = :id_lote";

					$stmt = $this->db->prepare($sql);
					$parametros = array(':id_lote' => $id);
					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
							$stmt->execute($parametros);
							return $stmt->fetch();
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function consultar_detalles_abonos($id_lote)
			{

					$sql = "SELECT * FROM abonos WHERE id_lote =:id_lote";
					$stmt = $this->db->prepare($sql);
					$parametros = array(':id_lote' => $id_lote);

					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
							$stmt->execute($parametros);
							return $stmt->fetchAll();

					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function consultar_todos_valor_abonos_lote($id_lote)
			{

					$sql = "SELECT valor_abono FROM abonos WHERE id_lote =:id_lote";
					$stmt = $this->db->prepare($sql);
					$parametros = array(':id_lote' => $id_lote);

					try {
							$stmt->execute($parametros);
							return $stmt->fetchAll();
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function eliminar_todos_abonos_id_lote($id_lote)
			{

					$sql = "DELETE FROM abonos WHERE id_lote = :id_lote";
					$stmt = $this->db->prepare($sql);
					$parametros = array(':id_lote' => $id_lote);

					try {
							$stmt->execute($parametros);
							if ($stmt->rowCount() == 1) {
									return true;
							}
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function eliminar_un_abono($id_abono)
			{

				$sql = "DELETE FROM abonos WHERE id = :id_abono";
				$stmt = $this->db->prepare($sql);
				$parametros = array(':id_abono' => $id_abono);

				try {
						$stmt->execute($parametros);
						if ($stmt->rowCount() == 1) {
								return true;
						}
				} catch (\Exception $e) {
						return $e->getCode();
				}

			}

			public function consultar_detalle_abono_id($id_abono)
			{

					$sql = "SELECT * FROM abonos WHERE id = :id";
					$stmt = $this->db->prepare($sql);
					$parametros = array(':id' => $id_abono);

					//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
					try {
							$stmt->execute($parametros);
							return $stmt->fetch();
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function actualizar_un_abono_desde_hoja_vida($data)
			{

					$sql = "UPDATE abonos SET
									numero_recibo = :numero_recibo,
									forma_abono_comprador = :forma_abono_comprador,
									valor_abono = :valor_abono,
									observaciones = :observaciones
									WHERE id = :id_abono";

					$parametros = array(':numero_recibo' => $data['recibo_abono_update'],
															':forma_abono_comprador' => $data['tipo_abono_update'],
															':valor_abono' => $data['valor_abono_update'],
															':observaciones' => $data['observaciones_update'],
															':id_abono' => $data['id_abono_update']);

					$stmt = $this->db->prepare($sql);
					
					try {
							$stmt->execute($parametros);

							if($stmt->rowCount() == 1){
									return true;
							} else{
									return false;
							}
					} catch (\Exception $e) {
							return $e->getCode();
					}
			}

			public function estimado_financiado($estado)
			{
				$sql = "SELECT DISTINCT
								l.numero_lote,
								l.id_tipo_lote,
								(SELECT CONCAT( nombres, ' ', apellidos ) FROM compradores WHERE id = a.id_comprador) as nombre,
								l.valor,
								l.numero_cuotas,
								l.valor_pagar,
								l.estado_lote
								FROM abonos as a
								INNER JOIN lotes as l ON l.id = a.id_lote
								WHERE l.estado_lote = :estado_lote
								AND l.numero_cuotas > 12";

				$stmt = $this->db->prepare($sql);
				$parametros = array(':estado_lote' => $estado);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
			}

			public function estimado_contado($estado)
			{
				$sql = "SELECT DISTINCT
								l.numero_lote,
								l.id_tipo_lote,
								(SELECT CONCAT( nombres, ' ', apellidos ) FROM compradores WHERE id = a.id_comprador) as nombre,
								l.valor,
								l.numero_cuotas,
								l.valor_pagar,
								l.estado_lote
								FROM abonos as a
								INNER JOIN lotes as l ON l.id = a.id_lote
								WHERE l.estado_lote = :estado_lote";

				$stmt = $this->db->prepare($sql);
				$parametros = array(':estado_lote' => $estado);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
			}

			public function estimado_financiado_sin_interes($estado)
			{
				$sql = "SELECT DISTINCT
								l.numero_lote,
								l.id_tipo_lote,
								(SELECT CONCAT( nombres, ' ', apellidos ) FROM compradores WHERE id = a.id_comprador) as nombre,
								l.valor,
								l.numero_cuotas,
								l.valor_pagar,
								l.estado_lote
								FROM abonos as a
								INNER JOIN lotes as l ON l.id = a.id_lote
								WHERE l.estado_lote = :estado_lote
								AND l.numero_cuotas = 12";

				$stmt = $this->db->prepare($sql);
				$parametros = array(':estado_lote' => $estado);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute($parametros);
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}
			}

			public function consultar_lotes_ultimo_pago()
			{
				$sql = "SELECT * FROM(
								SELECT 
								    l.id AS id_lote,
								    c.id AS id_comprador,
								    c.nombres,
								    c.apellidos,
								    c.celular,
								    c.dia_pago,
								    if(l.id_tipo_lote = 1,'Vivienda', 'Comercial') AS tipo_lote,
								    l.numero_lote,
								    l.valor_pagar,
								    MAX(a.fecha) as ultimo_pago,
								    c.dia_pago+5 as dia_aviso
								FROM
								    abonos AS a
								INNER JOIN lotes AS l
								ON
								    l.id = a.id_lote
								INNER JOIN compradores AS c
								ON
								    c.id = a.id_comprador
								WHERE
									l.estado_lote = 2
								GROUP BY
								    id_lote) AS t
								WHERE
									DATEDIFF(NOW(), ultimo_pago) >=31 AND day(now()) >= dia_aviso  
								ORDER BY `t`.`id_comprador`  DESC";

				$stmt = $this->db->prepare($sql);
				//echo'[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parametros);  exit();
				try {
						$stmt->execute();
						return $stmt->fetchAll();
				} catch (\Exception $e) {
						return $e->getCode();
				}


			}

}