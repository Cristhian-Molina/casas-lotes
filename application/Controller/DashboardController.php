<?php

namespace Casas_lotes\Controller;

class DashboardController{

		public function index()
		{
				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/index.php');
		}

		public function cerrar_session()
		{
				session_start();
				session_destroy();
				$casa = URL;
				header("location: $casa");
		}

		public function comprador()
		{
				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/nuevoComprador.php');
		}

		public function vendedores()
		{
				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/vendedores.php');
		}

		public function lotes()
		{
				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/lotes.php');

		}

		public function actualizar_datos_comprador()
		{

				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/actualizarDatosComprador.php');
		}

		public function desistimientos()
		{
				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/desistimientos.php');
		}

		public function estimado_financiado()
		{

				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/estimados/estimadoFinanciado.php');

		}

		public function estimado_sin_interes()
		{

				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/estimados/estimadoSinInteres.php');

		}

		public function estimado_contado()
		{

				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/estimados/estimadoContado.php');

		}

		public function estimado_disponibles()
		{

				session_start();
				$_SESSION['sesion_usuario'];
				view('dashboard/estimados/estimadoDisponibles.php');

		}
}