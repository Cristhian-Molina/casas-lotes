<?php


namespace Casas_lotes\Controller;
use Casas_lotes\Model\Abonos;

class LotesController
{

	private $_abonos;

	function __construct()
	{
			//$this->_compradores = new Compradores();
			$this->_abonos = new Abonos();
			//$this->_lotes = new Lotes();
	}

	public function hoja_lote($id)
	{

			session_start();
			$_SESSION['sesion_usuario'];
			
			//OBTIENE UNA CONSULTA ANIDADA DE DATA COMPRADOR Y REL_ASESOR Y LOTE
			$mostrar_data_comprador = $this->_abonos->consultar_data_abono_comprador($id);

			view('hojaVidaLote.php', $mostrar_data_comprador);
	}
}
