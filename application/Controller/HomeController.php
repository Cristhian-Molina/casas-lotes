<?php

namespace Casas_lotes\Controller;


class HomeController
{

    public function index()
    {
    session_start();
    if (!empty($SESSION['sesion_usuario'])) {
    	view('dashboard/');
    } else{

    	view('login/index.php');
    }

    }

}
