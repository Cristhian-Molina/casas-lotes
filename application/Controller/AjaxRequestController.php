<?php

namespace Casas_lotes\Controller;

use Casas_lotes\Model\Usuarios;
use Casas_lotes\Model\Vendedores;
use Casas_lotes\Model\Lotes;
use Casas_lotes\Model\Compradores;
use Casas_lotes\Model\Abonos;
use Casas_lotes\Model\RelAsesoresLotes;
use Casas_lotes\Model\Desistimientos;

class AjaxRequestController{

	private $_usuarios;
	private $_vendedores;
	private $_lotes;
	private $_compradores;
	private $_abonos;
	private $_rel_asesor_lote;
	private $_alertaCompradores;

	function __construct()
	{
			$this->_usuarios = new Usuarios();
			$this->_vendedores = new Vendedores();
			$this->_lotes = new Lotes();
			$this->_compradores = new Compradores();
			$this->_abonos = new Abonos();
			$this->_rel_asesor_lote = new RelAsesoresLotes();
	}

	function validar_form_login_user()
	{
			$login_usuario = $this->_usuarios->validar_form_login_usuario($_POST);
			echo json_encode($login_usuario);
	}

	function agregar_nuevo_vendedor()
	{
			session_start();
			$_SESSION['sesion_usuario'];
			$insertar_nuevo_vendedor = $this->_vendedores->agregar_nuevo_vendedor_model($_POST);
			echo json_encode($insertar_nuevo_vendedor);
	}

	function cargar_todos_vendedores()
	{
			$todos_vendedores = $this->_vendedores->mostar_todos_vendedores();
			if (!empty($todos_vendedores)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $todos_vendedores);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_un_vendedor($id)
	{
			$un_vendedor = $this->_vendedores->mostar_un_vendedor($id);
			if (!empty($un_vendedor)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $un_vendedor);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function actualizar_un_vendedor()
	{
			$actualizar_un_vendedor = $this->_vendedores->actualizar_un_vendedor($_POST);
			if ($actualizar_un_vendedor) {
					$resultado = array('estado_respuesta' => 1, 'data' => $actualizar_un_vendedor);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function eliminar_un_vendedor($id)
	{
			$eliminar_un_vendedor = $this->_vendedores->eliminar_un_vendedor($id);
			if ($eliminar_un_vendedor) {
					$resultado = array('estado_respuesta' => 1);
					echo json_encode($resultado);
			} else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_lotes_vivienda()
	{
		$lotes_vivienda = $this->_lotes->mostrar_lotes_vivienda();
		if (!empty($lotes_vivienda)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $lotes_vivienda);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}

	function cargar_un_lote_data_id($id, $tipo_lote)
	{
			$id_lote = $this->_lotes->mostrar_data_un_lote($id, $tipo_lote);
			if (!empty($id_lote)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $id_lote);
					echo json_encode($resultado);
			}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}

	function actualizar_estado_lote($id, $tipo_estado)
	{
			$actualizar_estado_lote = $this->_lotes->actualizar_estado_lote($id, $tipo_estado);
			if ($actualizar_estado_lote) {
					$resultado = array('estado_respuesta' => 1, 'data' => $actualizar_estado_lote);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function actualizar_un_lote_data()
	{

			$actualizar_lote = $this->_lotes->actualizar_un_lote_data($_POST);
			if ($actualizar_lote) {
					$resultado = array('estado_respuesta' => 1, 'data' => $actualizar_lote);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_lotes_comercial()
	{
		$lotes_comercial = $this->_lotes->mostrar_lotes_comercial();
		if (!empty($lotes_comercial)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $lotes_comercial);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}


	function agregar_nuevo_lote()
	{
			session_start();
			$_SESSION['sesion_usuario'];
			$insertar_nuevo_lote = $this->_lotes->agregar_nuevo_lote($_POST);
			echo json_encode($insertar_nuevo_lote);
	}

	function cargar_todos_lotes_seleccionado($id)
	{
			$lotes_cargados = $this->_lotes->mostrar_todos_lotes_seleccionado($id);
			if (!empty($lotes_cargados)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $lotes_cargados);
					echo json_encode($resultado);
			} else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_valor_lote_seleccionado()
	{
			$valor_lote_cargado = $this->_lotes->mostrar_valor_lote_seleccionado($_POST);
			if (!empty($valor_lote_cargado)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $valor_lote_cargado);
					echo json_encode($resultado);
			} else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function agregar_nuevo_comprador()
	{

			$insertar_nuevo_comprador = $this->_compradores->agregar_nuevo_comprador($_POST);
			echo json_encode($insertar_nuevo_comprador);
	}

	function eliminar_comprador($id_comprador)
	{
			$eliminar_comprador = $this->_compradores->eliminar_un_comprador($id_comprador);

			if ($eliminar_comprador) {
					$resultado = array('estado_respuesta' => 1);
					echo json_encode($resultado);
			} else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}


	function cargar_un_comprador()
	{

			$buscar_comprador = $this->_compradores->buscar_comprador_cedula($_POST['buscar_cedula']);
			if (!empty($buscar_comprador)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $buscar_comprador);
					echo json_encode($resultado);
			} else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_un_comprador_id($id)
	{
			$buscar_comprador_id = $this->_compradores->buscar_comprador_id($id);
			if (!empty($buscar_comprador_id)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $buscar_comprador_id);
					echo json_encode($resultado);
			}else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function actualizar_un_comprador()
	{

			$actualizar_comprador = $this->_compradores->actualizar_un_comprador($_POST);
			if ($actualizar_comprador) {
					$resultado = array('estado_respuesta' => 1, 'data' => $actualizar_comprador);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_un_comprador_lote()
	{

			$buscar_comprador_lote = $this->_compradores->buscar_comprador_lote($_POST);
			if (!empty($buscar_comprador_lote)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $buscar_comprador_lote);
					echo json_encode($resultado);
			}else{
				echo json_encode(array('estado_respuesta' => 0));
			}

	}

	function cargar_un_comprador_nombre()
	{

			$buscar_comprador_nombre = $this->_compradores->mostar_comprador_nombre($_POST);
			if (!empty($buscar_comprador_nombre)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $buscar_comprador_nombre);
					echo json_encode($resultado);
			}else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function agregar_datos_lote()
	{

			session_start();
			$_SESSION['sesion_usuario'];
			//CONSULTAR EL ID DEL LOTE EN LA TABLA LOTES
			$buscar_id_lote = $this->_lotes->mostrar_id_lote_por_tipo_y_numero($_POST['tipo_lote_valor'], $_POST['numero_lote_valor']);
			if ($_POST['tipo_pago'] == 1) {
					//AGREGAR INFORMACION DE CONTADO

					//AGREGAR INFORMACION TABLA ABONADOS
					$agregar_abono_contado = $this->_abonos->insertar_nuevo_abono_contado($_POST, $buscar_id_lote->id);
					//ACTUALIZAR DATOS EN LA TABLA LOTES DE CONTADO
					$actualizar_lote = $this->_lotes->actualizar_lote_despues_abono_contado($_POST, $buscar_id_lote->id);
					
			}elseif($_POST['tipo_pago'] == 2){
					//AGREGAR INFORMACION FINANCIADO
					//AGREGAR INFORMACION TABLA ABONADOS
					$agregar_abono_financiado = $this->_abonos->insertar_nuevo_abono_financiado_separado($_POST, $buscar_id_lote->id);
					//ACTUALIZAR DATOS EN LA TABLA LOTES DE FINANCIADO
					$actualizar_lote = $this->_lotes->actualizar_lote_despues_abono_financiado_separado($_POST, $buscar_id_lote->id, 2);
			} elseif ($_POST['tipo_pago'] == 3) {

					$_POST['abono'] = 0;
					//AGREGAR INFORMACION SEPARADO
					$agregar_separar_lote = $this->_abonos->insertar_nuevo_abono_financiado_separado($_POST, $buscar_id_lote->id);

					$actualizar_lote = $this->_lotes->actualizar_lote_despues_abono_financiado_separado($_POST, $buscar_id_lote->id, 3);
			}

			$agregar_rel_asesor_lote = $this->_rel_asesor_lote->ingresar_asesor_lote($_POST['cargar_asesores'], $buscar_id_lote->id);

	}

	function guardar_abono_lote()
	{

			session_start();
			$_SESSION['sesion_usuario'];

			//VALIDAR LA INSERCION DE LOS DATOS EN LA BASE DE DATOS
			$guardar_abono = $this->_abonos->insertar_siguiente_abono_finandiado($_POST);

			if ($guardar_abono) {

					//VARIABLE QUE CONTIENE UN ARRAY DE TODOS LO ABONOS DEL LOTE
					$comparar_abonos_total = $this->_abonos->consultar_todos_valor_abonos_lote($_POST['id_lote']);

					//VARIABLE QUE CONTIENE EL VALOR TOTAL DEL LOTE, QUITANDO EL FORMATO DE MONEDA
					$valor_total_lote = (int) $this->str_replaceChars($_POST['total_pagar_lote']);

					//VARIABLE QUE CONTIENE UN ARRAY DE TODOS LOS VALORES ABONADOS DEL LOTE
					$valor_abono = array_column($comparar_abonos_total, 'valor_abono');

					//VARIABLE DONDE SE ALMACENAN TODOS LOS VALORES ABONADOS DEL LOTE
					$array = array();

					//CICLO QUE RECORRE EL ARRAR DE CADA VALOR PARA APLICAR LA FUNCION QUE ELIMINA EL FORMATO DE MONEDA Y VOLVER LOS VALORES A NUMEROS ENTEROS
					foreach ($valor_abono as $value) {
							$v = $this->str_replaceChars($value);
							$array[] = (int) $v;
					}

					//VARIABLE QUE HACE LA SUMATORIA DE TODOS LOS VALORES
					$suma_abonos = array_sum($array);

					//CONDICIONAL DONDE COMPARAMOS LA SUMA DE LOS ABONOS CON EL VALOR DEL LOTE
					if ($suma_abonos >= $valor_total_lote) {

							//ACTUALIZAMOS EL ESTADO DEL LOTE A PAGADO, SI ES MAYOR O IGUAL A LA SUMATORIA DE TODOS LOS ABONOS
							$this->_lotes->actualizar_estado_lote($_POST['id_lote'], 4);
					}
					echo json_encode(array('estado_respuesta' => 1));
			}

	}

	function guardar_abono_lote_separado()
	{

		session_start();
		$_SESSION['sesion_usuario'];
		//ACTUALIZAR EL ESTADO DEL LOTE  Y LOS OTROS CAMPOS EN LA TABLA LOTES
		$actualizar_data_lote = $this->_lotes->actualizar_estado_lote_data_separado($_POST, 2);
		if($actualizar_data_lote){
				//INGRESAR VALORES EN LA TABLA ABONOS
				$ingresar_abono_separado = $this->_abonos->insertar_siguiente_abono_finandiado($_POST);
				echo json_encode($ingresar_abono_separado);
		}

	}

	function str_replaceChars($str)
	{
			$array = array('$ ', '.');
			$signal = str_replace($array,"",$str);
			
			return $signal;
	}

	function cargar_detalles_abonos($id_lote)
	{

			$buscar_abonos = $this->_abonos->consultar_detalles_abonos($id_lote);

			if (!empty($buscar_abonos)) {

					$resultado = array('estado_respuesta' => 1,
														 'data' => $buscar_abonos);
					echo json_encode($resultado);
			}else{
				echo json_encode(array('estado_respuesta' => 0));
			}
	}


	function desistir_lote($id_lote)
	{

			session_start();
			$_SESSION['sesion_usuario'];
			//CONSULTAR TODOS LOS ABONOS DE ESE LOTE
			$consultar_abonos = $this->_abonos->consultar_detalles_abonos($id_lote);

			//SACAR EL ID DEL COMPRADOR
			$id_comprador = $consultar_abonos[0]->id_comprador;

			//SOLO CAPTURAMOS EL VALOR DE LOS ABONOS
			$todos_abonos = array_column($consultar_abonos, 'valor_abono');

			$array = array();

			//SE RECORREN TODOS LOS ABONOS Y SE SETEAN PARA QUE QUEDE VALOR ENTERO
			foreach ($todos_abonos as $value) {
					$v = $this->str_replaceChars($value);
					$array[] = (int) $v;
			}

			$suma_abonos = array_sum($array);

			//INSTANCIAMOS EL MODELO DESISTIMIENTO
			$desistimiento = new Desistimientos();

			//INSERTAR LA SUMA DE LOS ABONOS A LA TABLA DESISTIMIENTOS
			$insertar_data_desistimiento = $desistimiento->insertar_data_desistimiento($id_lote, $id_comprador, $suma_abonos);

			if ($insertar_data_desistimiento['estado_respuesta'] == 1) {
					//ELIMINAR REGISTROS DE LA TABLA ABONO DE ACUERDO AL ID DEL LOTE
					$eliminar_abonos = $this->_abonos->eliminar_todos_abonos_id_lote($id_lote);

					//ELIMINAR REGISTROS DE LA TABLA REL_ASESOR DE ACUERDO AL ID DEL LOTE
					$eliminar_rel_asesor = $this->_rel_asesor_lote->eliminar_asesores_lote_id($id_lote);

					$data = array('tipo_pago' => 0, 'total_lote_financiado' =>0, 'lote_cuotas' => 0, 'valor_por_cuota' => 0);
					//ACTUALIZAR ESTADO DEL LOTE
					$actualizar_estado_lote = $this->_lotes->actualizar_lote_despues_abono_financiado_separado($data, $id_lote, 1);

					echo json_encode(array('estado_respuesta' => 1));
			}

	}

	function cargar_data_desistimientos()
	{

			//INSTANCIAMOS EL MODELO DESISTIMIENTO
			$desistimiento = new Desistimientos();
			$data_desistimiento = $desistimiento->consultar_data_desistimientos_data_comprador();

			if (!empty($data_desistimiento)) {

						$resultado = array('estado_respuesta' => 1, 'data' => $data_desistimiento);
						echo json_encode($resultado);
			}else{
						echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_cantidad_lotes_por_estado()
	{
			$cargar_data_lotes_por_estado = $this->_lotes->consultar_total_lotes_por_estado();

			if (!empty($cargar_data_lotes_por_estado)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $cargar_data_lotes_por_estado);
					echo json_encode($resultado);
			} else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_total_lotes_y_vendidos()
	{

			$cargar_total_lotes_y_vendidos = $this->_lotes->consultar_total_lotes_y_vendidos();
			if (!empty($cargar_total_lotes_y_vendidos)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $cargar_total_lotes_y_vendidos);
					echo json_encode($resultado);
			} else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function cargar_un_abono($id_abono)
	{

			$cargar_un_abono = $this->_abonos->consultar_detalle_abono_id($id_abono);
			if (!empty($cargar_un_abono)) {
					$resultado = array('estado_respuesta' => 1, 'data' => $cargar_un_abono);
					echo json_encode($resultado);
			} else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function actualizar_un_abono()
	{

			$actualizar_un_abono = $this->_abonos->actualizar_un_abono_desde_hoja_vida($_POST);
			if ($actualizar_un_abono) {
					$resultado = array('estado_respuesta' => 1, 'data' => $actualizar_un_abono);
					echo json_encode($resultado);
			}else{
					echo json_encode(array('estado_respuesta' => 0));
			}
	}

	function eliminar_un_abono($id_abono)
	{
			$eliminar_un_abono = $this->_abonos->eliminar_un_abono($id_abono);

			if ($eliminar_un_abono) {
					echo json_encode(array('estado_respuesta' => 1));
			}
	}

	function cargar_estimado_financiado()
	{

		$estimado_financiado = $this->_abonos->estimado_financiado(2);
		if (!empty($estimado_financiado)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $estimado_financiado);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}

	function cargar_estimado_contado()
	{

		$estimado_contado = $this->_abonos->estimado_contado(4);
		if (!empty($estimado_contado)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $estimado_contado);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}

	function cargar_estimado_financiado_sin_interes()
	{

		$estimado_sin_interes = $this->_abonos->estimado_financiado_sin_interes(2);
		if (!empty($estimado_sin_interes)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $estimado_sin_interes);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}

	function cargar_estimado_disponibles()
	{

		$estimado_disponibles = $this->_lotes->estimado_disponibles();
		if (!empty($estimado_disponibles)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $estimado_disponibles);
				echo json_encode($resultado);
		}else{
				echo json_encode(array('estado_respuesta' => 0));
		}
	}


	function cargar_ultimo_pago_lotes()
	{

		$ultimo_pago = $this->_abonos->consultar_lotes_ultimo_pago();

		if (!empty($ultimo_pago)) {
				$resultado = array('estado_respuesta' => 1, 'data' => $ultimo_pago);
				echo json_encode($resultado);
		} else{
			echo json_encode(array('estado_respuesta' => 0));
		}
	}

}