<?php

namespace Casas_lotes\Controller;

use Casas_lotes\Model\Compradores;
use Casas_lotes\Model\Abonos;
use Casas_lotes\Model\Lotes;

class CompradorController
{

	private $_compradores;
	private $_abonos;
	private $_lotes;
	function __construct()
	{
			$this->_compradores = new Compradores();
			$this->_abonos = new Abonos();
			$this->_lotes = new Lotes();
	}

	function compra_lote($id_comprador)
	{
		$validar_comprador = $this->_compradores->mostrar_un_comprador($id_comprador);

		if (empty($validar_comprador)) {
				session_start();
				$_SESSION['sesion_usuario'];
				$casa = URL.'dashboard/comprador/';
				header("location: $casa");
		}else{
				session_start();
				$_SESSION['sesion_usuario'];
				view('comprador/dataComprador.php', $validar_comprador);
		}
	}


	function abona_lote($id)
	{

		session_start();
		$_SESSION['sesion_usuario'];

		//OBTIENE UNA CONSULTA ANIDADA DE DATA COMPRADOR Y REL_ASESOR Y LOTE
		$mostrar_data_comprador = $this->_abonos->consultar_data_abono_comprador($id);

		//OBTIENE UNA CONSULTA DE LA DATA DEL LOTE POR SU ID Y TIPO DE LOTE
		$comprobar_estado_lote = $this->_lotes->mostrar_data_un_lote($mostrar_data_comprador->id_lote, $mostrar_data_comprador->id_tipo_lote);

		//SI ES IGUAL A 3 ES POR QUE SU ESTADO ES SEPARADO
		if ($comprobar_estado_lote->estado_lote == 3) {

				//CAPTURAMOS EL VALOR DEL LOTE EN LA VARIABLE
				$valor = $comprobar_estado_lote->valor;

				//VARIABLE QUE VA ALMACENAR TODA LA DATA ANIDADA DE LAS 3 TABLAS ANTERIORES
				$array_data = $mostrar_data_comprador;
				//SE AGREGA AL ARREGLO EL VALOR DEL LOTE BUSCADO
				$array_data->valor = $valor;

				view('comprador/calcularSeparado.php', $array_data);
			//SI ES IGUAL A 2 ES POR QUE SU ESTADO ES ABONADO
		} elseif ($comprobar_estado_lote->estado_lote == 2) {

				view('comprador/abonoLote.php', $mostrar_data_comprador);

		} elseif($comprobar_estado_lote->estado_lote == 4){

				$casa = URL.'dashboard/lotes/';
				header("location: $casa");
		}

	}

}
