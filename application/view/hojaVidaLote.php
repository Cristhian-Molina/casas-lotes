<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
	<title>Hoja de vida lote</title>
</head>
<body>

<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="container">
	<div class="columns mt-5">
		<div class="column is-8">
			<h4 class="is-size-3">Datos del comprador</h4>
		</div>
	</div>

	<div class="columns">
		<div class="column box mr-2">
			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="hidden" value="<?php echo $data->id_lote ?>" id="hidden_id_lote">
							<input type="text" class="input" value="<?php echo $data->nombres ?>" id="nombre_comprador"readonly>
						</div>
						<p class="help has-text-info">Nombre del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->apellidos ?>" id="apellidos_comprador" readonly>
						</div>
						<p class="help has-text-info">Apellidos del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_identificacion ?>" id="numero_cedula" readonly>
						</div>
						<p class="help has-text-info">Numero identificación del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->direccion_residencial ?>" readonly>
						</div>
						<p class="help has-text-info">Dirección del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->correo_electronico ?>" readonly>
						</div>
						<p class="help has-text-info">Correo electronico del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->celular ?>" readonly>
						</div>
						<p class="help has-text-info">Celular del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo ($data->id_tipo_lote == 1 ? 'Vivienda' : 'Comercial') ?>" readonly id="tipo_lote">
						</div>
						<p class="help has-text-info">Tipo del Lote</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_lote ?>" readonly id="numero_lote">
						</div>
						<p class="help has-text-info">Numero del lote</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->valor_pagar ?>" id="valor_lote" readonly>
						</div>
						<p class="help has-text-info">Valor A pagar del lote</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->cuota ?>" readonly>
						</div>
						<p class="help has-text-info">Cuota mensual establecida por el comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_cuotas ?>" readonly>
						</div>
						<p class="help has-text-info">Plazo de cuotas establecidas por el comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->asesor ?>" readonly>
						</div>
						<p class="help has-text-info">Vendido por</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="columns mt-6">
		<div class="column is-12">
			<h4 class="is-size-3">Hoja de vida del lote</h4>
		</div>
	</div>

	<div class="columns mb-6">
		<div class="column is-12 box" >
			<div style="max-height: 560px; overflow-y: scroll;">
				<table class="table is-fullwidth is-striped is-narrow">
					<thead>
						<tr>
							<th>Fecha</th>
							<th>Concepto</th>
							<th>Recibo</th>
							<th>Tipo abono</th>
							<th>Observaciones</th>
							<th>Valor del abono</th>
							<th>Saldo</th>
						</tr>
					</thead>
					<tbody id="cargar_abonos_id_lote"></tbody>
				</table>
			</div>
		</div>
	</div>


<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script type="module" src="<?= URL . 'public/js/app_hojaVidaLote.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>