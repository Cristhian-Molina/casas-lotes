<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/pikaday.css">
	<title>Datos del comprador</title>
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
			<a class="navbar-item" href="#">
				<img src="<?php echo URL ?>public/img/logo.svg" width="112" height="28">
			</a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
		</div>

		<div id="navbarBasicExample" class="navbar-menu">
			<div class="navbar-start">
				<div class="navbar-item">
					<div class="buttons">
						<a class="button is-warning" id="terminar_proceso" data-id="<?php echo $data->id ?>">
							<strong>Terminar el proceso</strong>
						</a>
					</div>
				</div>
				<div class="navbar-item">
					<div class="buttons">
						<a class="button is-primary" href="<?php echo URL ?>dashboard/cerrar_session/">
							<strong>Cerrar sesion</strong>
						</a>
					</div>
				</div>
			</div>

			<!--<div class="navbar-end">
			</div>-->
		</div>
	</nav>

<div class="container">
	<div class="columns">
		<div class="column is-4">
			<div class="column">
				<h2 class="title is-4">Datos del comprador reciente</h2>
			</div>
			<div class="column">
				<div class="box">
					<div class="field">
						<label for="" class="label">Nombres del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->nombres ?>" readonly>
						</div>
					</div>

					<div class="field">
						<label for="" class="label">Apellidos del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->apellidos ?>" readonly>
						</div>
					</div>

					<div class="field">
						<label for="" class="label">Numero identificación del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_identificacion ?>" readonly>
						</div>
					</div>

					<div class="field">
						<label for="" class="label">Dirección del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->direccion_residencial ?>" readonly>
						</div>
					</div>

					<div class="field">
						<label for="" class="label">Correo electrónico del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->correo_electronico ?>" readonly>
						</div>
					</div>

					<div class="field">
						<label for="" class="label">Celular del comprador</label>
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->celular ?>" readonly>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="column is-8">
			<div class="column">
				<h2 class="title is-4">Datos del lote</h2>
			</div>
			<div class="column">
				<div class="box">
					<form id="form_agregar_lote_comprador" autocomplete="off">
						<div class="field">
							<div class="columns">

								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="tipo_lote_valor" id="tipo_lote_valor">
												<option value="0">Seleccione...</option>
												<option value="1">Vivienda</option>
												<option value="2">Comercial</option>
											</select>
										</div>
										<p class="help has-text-link">Seleccione el tipo del lote</p>
									</div>
								</div>

								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="numero_lote_valor" id="numero_lote_valor">
												<option value="">Esperando Valor</option>
											</select>
										</div>
										<p class="help has-text-link">Numero del lote</p>
									</div>
								</div>

							</div>
						</div>

						<div class="field">
							<div class="columns">
								<div class="column is-half">
									<input type="text" class="input" name="valor_lote" id="valor_lote" readonly>
									<p class="help has-text-link">Valor del lote</p>
								</div>

								<div class="column is-half">
									<input type="text" class="input" name="fecha_abono" id="fecha_abono">
									<p class="help has-text-link">Fecha del Abono</p>
								</div>
							</div>

						</div>

						<div class="field">
							<label class="label">Forma de pago del lote</label>
							<div class="columns">
								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="tipo_pago" id="tipo_pago">
												<option value="0">Seleccione..</option>
												<option value="1">Contado</option>
												<option value="2">Financiado</option>
												<option value="3">Separar</option>
											</select>
										</div>
									</div>
									<p class="help has-text-link">Selecciona el método de pago por el lote</p>
								</div>
							</div>
						</div>

						<div class="mostrar_bloque_contado" style="display: none;">
							<div class="field">
								<div class="columns">
									<div class="column is-half">
										<div class="control">
											<input type="text" name="aplicar_descuento" id="aplicar_descuento" class="input">
											<p class="help has-text-link">Descuento para el lote</p>
										</div>
									</div>

									<div class="column is-half">
										<div class="control">
											<input type="text" name="total_lote_descuento" id="total_lote_descuento" class="input" readonly>
											<p class="help has-text-link">Total lote con descuento aplicado</p>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="columns">
									<div class="column is-half">
										<div class="control">
											<input type="text" name="valor_descontado" id="valor_descontado" class="input" readonly>
											<p class="help has-text-link">Este es el precio que se descontara del porcentaje aplicado</p>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="field mostrar_bloque_financiado" style="display: none;">
							<div class="columns">

								<div class="column is-half">
									<div class="field has-addons">
										<div class="control is-expanded">
											<input type="text" class="input" id="valor_interes" name="valor_interes">
											<p class="help has-text-info">Indique el % de la cuota inicial</p>
										</div>
									</div>
								</div>

								<div class="column is-half">
									<div class="field has-addons">
										<div class="control is-expanded">
											<input type="text" class="input" name="abono">
											<p class="help has-text-info">Abono</p>
										</div>
									</div>
								</div>

							</div>

							<div class="columns">
								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="lote_cuotas" id="lote_cuotas"></select>
										</div>
										<p class="help has-text-link">Numero de coutas establecidas</p>
									</div>
								</div>

								<div class="column is-half">
									<div class="control">
										<input type="text" class="input" name="total_deuda" readonly>
										<p class="help has-text-info">Total deuda</p>
									</div>
								</div>
							</div>

							<div class="columns">
								

								<div class="column is-half">
									<div class="field has-addons">
										<div class="control is-expanded">
											<input type="text" class="input" name="valor_por_cuota" readonly>
											<p class="help has-text-info">Valor por cuota Mensual</p>
										</div>
										<!--<a class="button is-primary" id="calcular_cuotas_mensuales">Generar</a>-->
									</div>
								</div>

								<div class="column is-half">
									<div class="control">
										<input type="text" class="input" name="total_lote_financiado" readonly>
										<p class="help has-text-info">Total lote financiado</p>
									</div>
								</div>
							</div>
						</div>

						<div class="field">
							<label class="label">Información del asesor</label>
							<div class="columns">
								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="cargar_asesores" id="cargar_asesores">
												<option class="eror-data" style="display: none;">No hay asesores por mostrar..</option>
												<option value="">selecciona...</option>
											</select>
											<p class="help has-text-link">Elige el asesor que le vendió al comprador</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="columns" style="margin-top: 30px;">
							
							<div class="column is-half">
								<div class="field">
									<div class="control">
										<textarea name="observaciones" id="observaciones" class="textarea"></textarea>
										<p class="help has-text-link">Describe cualquier observación adicional</p>
									</div>
								</div>
							</div>

							<div class="column is-half">
								<div class="field">
									<div class="control">
										<input type="hidden" name="id_comprador" value="<?php echo $data->id ?>">
										<input type="text" class="input" name="numero_recibo">
										<p class="help has-text-link">Digite el numero del recibo del comprador</p>
									</div>
									<div class="control">
										<input type="text" class="input" name="concepto">
										<p class="help has-text-link">Escriba el concepto</p>
									</div>
								</div>
							</div>
						</div>

						<div class="columns medio_pago">
							<div class="column">
								<div class="field">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="tipo_abono">
												<option value="0">Seleccione...</option>
												<option value="1">Efectivo</option>
												<option value="2">Bancolombia - #2175</option>
												<option value="3">Bancolombia - #1063</option>
												<option value="4">Banco de Occidente - #6391</option>
												<option value="5">Banco de Occidente - #6383</option>

											</select>
											<p class="help has-text-info">Selecciona el Medio de pago</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="field mt-6">
							<button class="button is-medium is-primary is-fullwidth">Enviar</button>
						</div>
					</form>

				</div><!--END BOX-->
			</div>
		</div>
	</div>
</div>

	<script type="text/javascript">
		var url_javascript = '<?= URL; ?>';
	</script>
	<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
	<script src="<?= URL; ?>public/js/helpers/pikaday.js"></script>
	<script type="module" src="<?= URL . 'public/js/app_dataComprador.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>