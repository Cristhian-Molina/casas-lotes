<?php
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/pikaday.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
	<title>Document</title>
</head>
<body>

<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="container">
	<div class="columns mt-5">
		<div class="column is-6">
			<h4 class="is-size-3">Datos del comprador</h4>
		</div>

		<div class="column is-6">
			<h4 class="is-size-3">Datos del Lote</h4>
		</div>
	</div>

	<div class="columns">
		<div class="column is-6 box mr-2">
			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->nombres ?>" readonly>
						</div>
						<p class="help has-text-info">Nombre del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->apellidos ?>" readonly>
						</div>
						<p class="help has-text-info">Apellidos del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_identificacion ?>" readonly>
						</div>
						<p class="help has-text-info">Numero identificación del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->direccion_residencial ?>" readonly>
						</div>
						<p class="help has-text-info">Dirección del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->correo_electronico ?>" readonly>
						</div>
						<p class="help has-text-info">Correo electronico del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->celular ?>" readonly>
						</div>
						<p class="help has-text-info">Celular del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo ($data->id_tipo_lote == 1 ? 'Vivienda' : 'Comercial') ?>" readonly>
						</div>
						<p class="help has-text-info">Tipo del Lote</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_lote ?>" readonly>
						</div>
						<p class="help has-text-info">Numero del lote</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->asesor ?>" readonly>
						</div>
						<p class="help has-text-info">Vendido por</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" id="valor_lote" value="<?php echo $data->valor ?>" readonly>
						</div>
						<p class="help has-text-info">Valor del lote</p>
					</div>
				</div>
			</div>
		</div>

		<div class="column is-6 box ml-2">
			<form id="form_calcular_lote">
				<div class="field">
					<div class="columns">

						<div class="column is-half">
							<div class="field has-addons">
								<div class="control is-expanded">
									<input type="text" class="input" id="valor_interes" name="valor_interes">
									<p class="help has-text-info">Indique el % de la cuota inicial</p>
								</div>
							</div>
						</div>

						<div class="column is-half">
							<div class="field has-addons">
								<div class="control is-expanded">
									<input type="text" class="input" name="valor_abonado">
									<p class="help has-text-info">Abono</p>
								</div>
							</div>
						</div>

					</div>

					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<div class="select is-fullwidth">
									<select name="lote_cuotas" id="lote_cuotas"></select>
								</div>
								<p class="help has-text-link">Numero de coutas establecidas</p>
							</div>
						</div>

						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="total_deuda" readonly>
								<p class="help has-text-info">Total deuda</p>
							</div>
						</div>
					</div>

					<div class="columns">
						<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="valor_por_cuota" readonly>
									<p class="help has-text-info">Valor por cuota Mensual</p>
								</div>
						</div>

						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="total_lote_financiado" readonly>
								<p class="help has-text-info">Total lote financiado</p>
							</div>
						</div>
					</div>

					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="recibo_abono">
							</div>
							<p class="help has-text-info">Recibo del abono</p>
						</div>

						<div class="column is-half">
							<div class="control">
								<div class="select is-fullwidth">
									<select name="tipo_abono">
										<option value="0">Seleccione..</option>
										<option value="1">Efectivo</option>
										<option value="2">Bancolombia</option>
										<option value="3">Occidente</option>
									</select>
								</div>
							</div>
							<p class="help has-text-info">Tipo de Abono</p>
						</div>
					</div>

					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="fecha_abono" id="fecha_abono">
							</div>
							<p class="help has-text-info">fecha del abono</p>
						</div>

						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="concepto_abono">
							</div>
							<p class="help has-text-info">Concepto</p>
						</div>
					</div>

					<div class="columns">
						<div class="column">
							<div class="control">
								<textarea name="observaciones" class="textarea"></textarea>
							</div>
							<p class="help has-text-link">Describe cualquier observación adicional</p>
						</div>
					</div>

					<div class="columns">
						<div class="column">
							<input type="hidden" value="<?php echo $data->id_comprador ?>" name='id_comprador'>
							<input type="hidden" value="<?php echo $data->id_lote ?>" name='id_lote'>
							<button class="button is-primary is-medium is-fullwidth">Enviar</button>
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
<script src="<?= URL; ?>public/js/helpers/pikaday.js"></script>
<script type="module" src="<?= URL . 'public/js/app_calcularSeparado.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>