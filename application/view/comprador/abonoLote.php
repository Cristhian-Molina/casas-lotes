<?php
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/pikaday.css">
	<title>Abonar lote</title>
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>

<?php require_once( APP. 'view/_templates/nav-header.php'); ?>


<div class="container">
	<div class="columns mt-5">
		<div class="column is-6">
			<h4 class="is-size-3">Datos del comprador</h4>
		</div>

		<div class="column is-6">
			<h4 class="is-size-3">Datos del Lote</h4>
		</div>
	</div>

	<div class="columns">
		<div class="column is-6 box mr-2">
			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->nombres ?>" id="nombre_comprador"readonly>
						</div>
						<p class="help has-text-info">Nombre del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->apellidos ?>" id="apellidos_comprador" readonly>
						</div>
						<p class="help has-text-info">Apellidos del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_identificacion ?>" id="numero_cedula" readonly>
						</div>
						<p class="help has-text-info">Numero identificación del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->direccion_residencial ?>" readonly>
						</div>
						<p class="help has-text-info">Dirección del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->correo_electronico ?>" readonly>
						</div>
						<p class="help has-text-info">Correo electronico del comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->celular ?>" readonly>
						</div>
						<p class="help has-text-info">Celular del comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo ($data->id_tipo_lote == 1 ? 'Vivienda' : 'Comercial') ?>" readonly id="tipo_lote">
						</div>
						<p class="help has-text-info">Tipo del Lote</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_lote ?>" readonly id="numero_lote">
						</div>
						<p class="help has-text-info">Numero del lote</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->valor_pagar ?>" id="valor_lote" readonly>
						</div>
						<p class="help has-text-info">Valor A pagar del lote</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->cuota ?>" readonly>
						</div>
						<p class="help has-text-info">Cuota mensual establecida por el comprador</p>
					</div>
				</div>
			</div>

			<div class="field">
				<div class="columns">
					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->numero_cuotas ?>" readonly>
						</div>
						<p class="help has-text-info">Plazo de cuotas establecidas por el comprador</p>
					</div>

					<div class="column is-half">
						<div class="control">
							<input type="text" class="input" value="<?php echo $data->asesor ?>" readonly>
						</div>
						<p class="help has-text-info">Vendido por</p>
					</div>
				</div>
			</div>
		</div>

		<div class="column is-6 box ml-2">
			<form id="form_agregar_nuevo_abono" autocomplete="off">
				<div class="field">
					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="fecha_abono" id="fecha_abono">
							</div>
							<p class="help has-text-info">fecha del abono</p>
						</div>

						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="concepto_abono">
							</div>
							<p class="help has-text-info">Concepto</p>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="recibo_abono">
							</div>
							<p class="help has-text-info">Recibo del abono</p>
						</div>

						<div class="column is-half">
							<div class="control">
								<div class="select is-fullwidth">
									<select name="tipo_abono">
										<option value="0">Seleccione...</option>
										<option value="1">Efectivo</option>
										<option value="2">Bancolombia - #2175</option>
										<option value="3">Bancolombia - #1063</option>
										<option value="4">Banco de Occidente - #6391</option>
										<option value="5">Banco de Occidente - #6383</option>
									</select>
								</div>
							</div>
							<p class="help has-text-info">Tipo de Abono</p>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="columns">
						<div class="column is-half">
							<div class="control">
								<input type="text" class="input" name="valor_abonado">
							</div>
							<p class="help has-text-info">Valor del abono</p>
						</div>

						<div class="column is-half">
							<div class="control">
								<textarea name="observaciones" class="textarea"></textarea>
							</div>
							<p class="help has-text-link">Describe cualquier observación adicional</p>
						</div>
					</div>
				</div>

				<div class="field">
					<div class="columns">
						<div class="column">
							<input type="hidden" value="<?php echo $data->id_comprador ?>" name='id_comprador'>
							<input type="hidden" value="<?php echo $data->id_lote ?>" name='id_lote'>
							<input type="hidden" name="total_pagar_lote" value="<?php echo $data->valor_pagar ?>">
							<button class="button is-primary is-medium is-fullwidth">Enviar</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>


	<div class="columns mt-6">
		<div class="column is-12">
			<h4 class="is-size-3">Hoja de vida del lote</h4>
		</div>
	</div>

	<div class="columns mx3">
		<div class="column is-2">
			<button class="button is-success is-small is-fullwidth" id="btn_generar_pdf">Generar Reporte</button>
		</div>

		<div class="column is-8">
			<div class="button is-link is-small is-fullwidth" style="cursor: none;">
				La suma de los abonos es de <strong id="suma_abonos"></strong>
			</div>
		</div>
	</div>

	<div class="columns mb-6">
		<div class="column is-12 box" >
			<div style="max-height: 560px; overflow-y: scroll;">
				<table class="table is-fullwidth is-striped is-narrow">
					<thead>
						<tr>
							<th>Fecha</th>
							<th>Concepto</th>
							<th>Recibo</th>
							<th>Tipo abono</th>
							<th>Observaciones</th>
							<th>Valor del abono</th>
							<th>Saldo</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody id="cargar_detalle_abonos"></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--ESTRUCTURA DE ACTUALIZACION MODAL-->
<div class="modal">
	<div class="modal-background"></div>
	<div class="modal-card">
		<form id="form_actualizar_abonos" autocomplete="off">
			<header class="modal-card-head">
				<p class="modal-card-title">Actualizar Abono</p>
				<button type='button' class="cancel-update delete" aria-label="close"></button>
			</header>
			<section class="modal-card-body">
				<div class="content">

						<div class="field">
							<label class="label">Recibo</label>
							<div class="control">
								<input class="input" type="text" name="recibo_abono_update">
							</div>
						</div>

						<div class="field">
							<label for="" class="label">Tipo del abono</label>
							<div class="select is-fullwidth">
								<select name="tipo_abono_update">
									<option value="0">Seleccione...</option>
									<option value="1">Efectivo</option>
									<option value="2">Bancolombia - #2175</option>
									<option value="3">Bancolombia - #1063</option>
									<option value="4">Banco de Occidente - #6391</option>
									<option value="5">Banco de Occidente - #6383</option>
								</select>
							</div>
						</div>

						<div class="field">
							<label class="label">Valor del abono</label>
							<div class="control">
								<input class="input" type="text" name="valor_abono_update">
							</div>
						</div>

						<div class="field">
							<label class="label">Observaciones</label>
							<div class="is-fullwidth">
								<textarea class="textarea" name="observaciones_update"></textarea>
							</div>
						</div>

				</div>
				

			</section>
			<footer class="modal-card-foot">
				<input type="hidden" name="id_abono_update">
				<button class="button is-success">Actualizar cambios</button>
				<a href="#" class="button is-danger cancel-update">Cancelar</a>
			</footer>
		</form>
	</div>
</div>
<!--FIN ESTRUCTURA MODAL ACTUALIZAR-->

<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
<script src="<?= URL; ?>public/js/helpers/pikaday.js"></script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.umd.js"></script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.plugin.autotable.js"></script>
<script type="module" src="<?= URL . 'public/js/app_abonoLote.js?version=' . microtime(); ?> "></script>

</body>
</html>

<?php endif; ?>