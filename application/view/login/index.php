<?php
	if (!empty($_SESSION['sesion_usuario'])) :
			$casa = URL.'dashboard/index';
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/login.css">
	<title>Inicio de Sesion | Casas & Lotes</title>
</head>
<body>

	<section class="container">
		<div class="columns is-multiline">
			<div class="column is-8 is-offset-2 register">
				<div class="columns">
					<div class="column left">
						<img src="<?php echo URL ?>public/img/logo-puertas-del-sol.jpg">
						
					</div>
					<div class="column right has-text-centered">
						<h1 class="title is-4">Inicia sesión ahora</h1>
						<p class="description">Recuerda no compartir los datos con nadie</p>
						<form id="form_login_user">
							<div class="field">
								<div class="control">
									<input class="input is-medium" type="text" placeholder="Nombre de Usuario" name="user" id="user">
								</div>
							</div>

							<div class="field">
								<div class="control">
									<input class="input is-medium" type="password" placeholder="Contraseña del usuario" name="password" id="password">
								</div>
							</div>
							<button class="button is-block is-primary is-fullwidth is-medium">Iniciar</button>
							<br>
							<small><em>Desarrollo exclusivo para la constructora Casas&Lotes.</em></small>
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<script type="text/javascript">
		var url_javascript = '<?= URL; ?>';
	</script>
	<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
	<script type="module" src="<?= URL . 'public/js/app_login.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>