<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<title>Desistimientos</title>
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>

<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="container mt-6">
	<div class="columns">
		<div class="column is-6">
			<h2 class="title is-4">Desistimientos</h2>
		</div>
	</div>

	<div class="columns">
		<div class="column is-12">
			<button class="button is-success is-small" id="btn_generar_pdf">Generar Reporte de Desistimientos</button>
		</div>
	</div>

	<div class="columns">
		<div class="column is-12">
			<div class="error" style="display: none;">
				<p class="error-texto has-text-danger">No hay Desistimientos por mostrar</p>
			</div>
			<div style="max-height: 960px; overflow-y: scroll;">
				<table class="table is-fullwidth is-striped is-narrow">
					<thead>
						<tr>
							<th>Fecha</th>
							<th># Lote</th>
							<th>Tipo del Lote</th>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>Numero Cédula</th>
							<th>Celular</th>
							<th>Total Abonado</th>
						</tr>
					</thead>
					<tbody id="cargar_desistimientos"></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="container mt-6">
	<div class="notification is-primary">
		<table class="table is-fullwidth">
			<thead>
				<tr class="has-text-centered">
					<th>Valor Total Desistimientos</th>
				</tr>
			</thead>
			<tbody>
				<tr class="has-text-centered">
					<td class="total_abonados"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.umd.js"></script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.plugin.autotable.js"></script>
<script type="module" src="<?= URL . 'public/js/app_desistimientos.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>