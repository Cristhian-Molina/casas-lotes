<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | casas && lotes</title>
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/vendedores.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>
	<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

	<div class="container mt-6">
		<div class="columns">
			<div class="column is-4 mr-6">
				<h2 class="title is-4">Agregar un nuevo Asesor</h2>
				<form class="box" id="nuevo_vendedor_form" autocomplete="off">

					<div class="field">
						<label for="" class="label">Nombres</label>
						<div class="control">
							<input type="text" class="input" name="nombres">
						</div>
					</div>

					<div class="field">
							<label for="" class="label">Apellidos</label>
							<div class="control">
								<input type="text" class="input" name="apellidos">
							</div>
					</div>

					<div class="field">
						<label for="" class="label">Tipo de documento</label>
						<div class="select is-fullwidth">
							<select name="tipo_documento">
								<option>Seleccione..</option>
								<option value="1">Cédula de ciudadanía</option>
								<option value="2">Cédula de extranjería</option>
								<option value="3">Pasaporte</option>
							</select>
						</div>
					</div>

					<div class="field">
							<label for="" class="label">Numero de Identificacion</label>
							<div class="control">
								<input type="text" class="input" name="numero_identificacion">
							</div>
					</div>

					<div class="field">
							<label for="" class="label">Celular</label>
							<div class="control">
								<input type="text" class="input" name="celular">
							</div>
					</div>

					<div class="field mt-5">
						<div class="control">
							<button class="button is-medium is-fullwidth is-primary">Crear</button>
						</div>
					</div>
				</form>
			</div>

			<div class="column is-6 ml-6">
				<h2 class="title is-4">Lista de Asesores</h2>
				<div class="card events-card">
					<div class="card-header">
						<p class="card-header-title">Asesores</p>
					</div>
					<div class="card-table">
						<div class="content">
							<div class="error" style="display: none;">
								<p class="error-texto has-text-danger">No hay asesores para mostrar</p>
							</div>
							<table class="table is-fullwidth is-striped">
								<tbody id="cargar_vendedores">
								</tbody>
							</table>
						</div>
					</div>
					<div class="card-footer">
						<a href="#" class="card-footer-item">Casas & Lotes</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!--ESTRUCTURA DE ACTUALIZACION MODAL-->
	<div class="modal">
		<div class="modal-background"></div>
		<div class="modal-card">
			<form id="actualizar_vendedores">
				<header class="modal-card-head">
					<p class="modal-card-title">Actualizar Datos del asesor</p>
					<button type='button' class="cancel-update delete" aria-label="close"></button>
				</header>
				<section class="modal-card-body">
					<div class="content">

							<div class="field">
								<label class="label">Nombres</label>
								<div class="control">
									<input class="input" type="text" name="nombres_update">
								</div>
							</div>

							<div class="field">
								<label class="label">Apellidos</label>
								<div class="control">
									<input class="input" type="text" name="apellidos_update">
								</div>
							</div>

							<div class="field">
								<label for="" class="label">Tipo de documento</label>
								<div class="select is-fullwidth">
									<select name="tipo_documento_update">
										<option>Seleccione..</option>
										<option value="1">Cédula de ciudadanía</option>
										<option value="2">Cédula de extranjería</option>
										<option value="3">Pasaporte</option>
									</select>
								</div>
							</div>

							<div class="field">
								<label class="label">Numero de Identificación</label>
								<div class="control">
									<input class="input" type="text" name="numero_identificacion_update">
								</div>
							</div>

							<div class="field">
								<label class="label">Celular</label>
								<div class="control">
									<input class="input" type="text" name="celular_update">
								</div>
							</div>

					</div>
					

				</section>
				<footer class="modal-card-foot">
					<input type="hidden" name="id_vendedor_update">
					<button class="button is-success">Actualizar cambios</button>
					<a href="#" class="button is-danger cancel-update">Cancelar</a>
				</footer>
			</form>
		</div>
	</div>
	<!--FIN ESTRUCTURA MODAL ACTUALIZAR-->

	<script type="text/javascript">
		var url_javascript = '<?= URL; ?>';
	</script>
	<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
	<script type="module" src="<?= URL . 'public/js/app_vendedor.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>