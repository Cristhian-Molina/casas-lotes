<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<title>Actualizar datos del comprador | casas&lotes</title>
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>
	<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="container mt-6">
	<div class="columns">
		<div class="column is-6">
			<h2 class="title is-4">Buscar el comprador por un dato</h2>
			<div id="bloque_buscar_comprador" class="box">

				<div class="columns">
					<div class="column">
						<div class="field">
							<div class="control">
								<div class="select is-fullwidth">
									<select id="select_tipo_busqueda">
										<option value="0">selecccione...</option>
										<option value="1">Por numero de cedula</option>
										<option value="2">Por numero de lote</option>
										<option value="3">Por nombres</option>
									</select>
								</div>
								<p class="help has-text-link">Seleccione la option para poder buscar</p>
							</div>
						</div>

					</div>
				</div>

				<div class="columns" id="bloque_buscar_cedula" style="display: none;">
					<form id="form_buscar_cedula">
						<div class="column">
							<div class="field has-addons">
								<div class="control">
										<input type="text" class="input" name="buscar_cedula">
										<p class="help has-text-link">Digite la cedula del comprador</p>
								</div>
								<div class="control">
									<button class="button is-primary">Buscar</button>
								</div>
							</div>
						</div>
					</form>
				</div>

				<div id="bloque_buscar_lote" style="display: none;">
					<form id="form_buscar_lote">
						<div class="field">
							<div class="columns">

								<div class="column is-half">
									<div class="control">
										<div class="select is-fullwidth">
											<select name="buscar_tipo_lote">
												<option value="0">Seleccione..</option>
												<option value="1">Vivienda</option>
												<option value="2">Comercial</option>
											</select>
										</div>
										<p class="help has-text-info">Indique que tipo de lote es</p>
									</div>
								</div>

								<div class="column is-half">
									<div class="control">
										<input type="text" class="input" name="buscar_lote" placeholder="Numero del lote">
										<p class="help has-text-info">Esciba el numero del lote</p>
									</div>
								</div>

							</div>
						</div>

						<div class="field">
							<div class="columns">
								<div class="column">
									<div class="control">
										<button class="button is-primary is-fullwidth is-medium">Buscar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>

				<div id="bloque_buscar_nombre" style="display: none">
						<div class="column">
							<div class="field">
								<div class="control">
										<input type="text" class="input" name="buscar_nombre" id="buscar_nombre_input" placeholder="Digite al menos 3 letras para hacer la busqueda">
										<p class="help has-text-link">Digite el nombre del comprador</p>
								</div>
							</div>
						</div>
				</div>
				

			</div>
		</div>

		<div class="column is-6 mt-6">
			<div class="box">
				<div class="error" style="display: block;">
					<p class="error-texto">No hay comprador que mostrar</p>
				</div>
				<table class="table is-fullwidth is-striped">
					<tbody id="mostrar_resultado_busqueda"></tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<!--ESTRUCTURA DE ACTUALIZACION MODAL-->
<div class="modal">
	<div class="modal-background"></div>
	<div class="modal-card">
		<form id="form_actualizar_comprador">
			<div class="modal-card-head">
				<p class="modal-card-title">Actualizar Datos del Comprador</p>
				<button type='button' class="cancel-update delete" aria-label="close"></button>
			</div>
			<div class="modal-card-body" style="max-height: 450px; overflow-y: scroll;">
				<div class="content">

						<div class="field">
							<label class="label">Nombres</label>
							<div class="control">
								<input class="input" type="text" name="nombres_update">
							</div>
						</div>

						<div class="field">
							<label class="label">Apellidos</label>
							<div class="control">
								<input class="input" type="text" name="apellidos_update">
							</div>
						</div>

						<div class="field">
							<label for="" class="label">Tipo de documento</label>
							<div class="select is-fullwidth">
								<select name="tipo_documento_update">
									<option>Seleccione..</option>
									<option value="1">Cédula de ciudadanía</option>
									<option value="2">Cédula de extranjería</option>
									<option value="3">Pasaporte</option>
								</select>
							</div>
						</div>

						<div class="field">
							<label class="label">Numero de Identificación</label>
							<div class="control">
								<input class="input" type="text" name="numero_identificacion_update">
							</div>
						</div>

						<div class="field">
							<label class="label">Direccion residencial</label>
							<div class="control">
								<input class="input" type="text" name="direccion_update">
							</div>
						</div>

						<div class="field">
							<label class="label">Correo Electronico</label>
							<div class="control">
								<input type="text" class="input" name="correo_update">
							</div>
						</div>

						<div class="field">
							<label class="label">celular</label>
							<div class="control">
								<input type="text" class="input" name="celular_update">
							</div>
						</div>

						<div class="field">
							<label for="" class="label">Dia de pago de los abonos</label>
							<div class="select is-fullwidth">
								<select name="dia_pago_update" id="dia_pago_update">
								</select>
							</div>
						</div>


				</div>

			</div>
			<div class="modal-card-foot">
				<input type="hidden" name="id_comprador_update">
				<button class="button is-success">Actualizar cambios</button>
				<a href="#" class="button is-danger cancel-update">Cancelar</a>
			</div>
		</form>
	</div>
</div>
<!--FIN ESTRUCTURA MODAL ACTUALIZAR-->


<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
<script type="module" src="<?= URL . 'public/js/app_actualizarComprador.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>