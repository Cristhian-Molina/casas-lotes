<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | casas && lotes</title>
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
		.progress-wrapper {
		  position: relative;
		}

		.progress-value {
		  position: absolute;
		  top: 0;
		  left: 50%;
		  transform: translateX(-50%);
		  font-size: calc(1rem / 1.5);
		  line-height: 1rem;
		  font-weight: bold;
		}
		.progress.is-large+.progress-value {
		  font-size: calc(1.5rem / 1.5);
		  line-height: 1.5rem;
		}
	</style>
</head>
<body>
<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="container mt-6">

	<div class="container">

		<div class="columns">
			<div class="column">
				<h4 class="title is-4">Porcentaje de Ventas en Lotes</h4>
			</div>

			<div class="column has-text-centered">
				<h4 class="title is-4">Total Ventas</h4>
			</div>
		</div>

		<div class="columns">
			<div class="column">
				<div class="progress-wrapper"></div>

				<!-- ESTRUCTURA QUE MUESTRA LAS ALERTAS DE LOTES EN MORA -->
						<div class="column" style="display: block;">
								<div class="card">
									<div class="card-header">
										<p class="card-header-title">Listado Personas en Mora de mas de 5 Días</p>
									</div>
									<div class="card-content" style="max-height: 280px;overflow-y: scroll;">

										<div id="alerta_mora"></div>
									</div>
								</div>
						</div>
			</div>

			<div class="column">
				<canvas id="lotes-por-estado"></canvas>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?= URL; ?>public/js/helpers/chart.js"></script>


<script type="module" src="<?= URL . 'public/js/app_index.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>