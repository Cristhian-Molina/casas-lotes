<?php
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<title>Document</title>
</head>
<body>

<?php require_once( APP. 'view/_templates/nav-header.php'); ?>

<div class="columns mt-6">
	<div class="column is-12">
		<h4 class="title is-4 has-text-centered">Estimado de ventas por Estado No vendidos
</h4>
	</div>
</div>

<div class="container">
	<div class="columns">
		<div class="column is-12">
			<h2 class="title is-6">Estimado por Lote <strong class="has-text-success">No vendidos</strong></h2>
			<button class="button is-success is-small" id="btn_generar_pdf">Generar Reporte del estimado de no vendidos</button>
		</div>
	</div>

	<div class="columns">
		<div class="column is-12" style="max-height: 560px; overflow-y: scroll;">
			<div class="error" style="display: none;">
				<p class="error-texto has-text-danger">No hay Desistimientos por mostrar</p>
			</div>
			<table class="table is-fullwidth is-striped is-narrow">
				<thead>
					<tr>
						<th># lote</th>
						<th>Tipo del lote</th>
						<th>Cliente</th>
						<th>Valor del lote</th>
					</tr>
				</thead>
				<tbody class="cargar_estimado_disponibles"></tbody>
			</table>
		</div>
	</div>
</div>

<div class="container mt-6">
	<div class="notification is-primary">
		<table class="table is-fullwidth">
			<thead>
				<tr class="has-text-centered">
					<th>Total Valor lote</th>
				</tr>
			</thead>
			<tbody>
				<tr class="has-text-centered">
					<td class="total_lotes"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var url_javascript = '<?= URL; ?>';
</script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.umd.js"></script>
<script src="<?php echo URL ?>public/js/helpers/jspdf.plugin.autotable.js"></script>
<script type="module" src="<?= URL . 'public/js/app_estimadoDisponibles.js?version=' . microtime(); ?> "></script>
</body>
</html>
<?php endif; ?>