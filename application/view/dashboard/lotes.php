<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
	<title>Dashboard | casas && lotes</title>
</head>
<body>
<?php require_once( APP. 'view/_templates/nav-header.php'); ?>


<div class="container mt-6">
	<div class="columns">
		<div class="column is-4">
			<h2 class="title is-4">Agregar un nuevo Lote</h2>
		</div>
	</div>

	<div class="columns">
		<div class="column is-6">
			<div class="box">
				<form id="form_nuevo_lote" autocomplete="off">
					<div class="field">
						<div class="columns">
							<div class="column is-half">
								<div class="control">
									<div class="select is-fullwidth">
										<select name="tipo_lote" id="tipo_lote">
											<option value="0">Seleccione...</option>
											<option value="1">Vivienda</option>
											<option value="2">Comercial</option>
										</select>
									</div>
									<p class="help has-text-link">Seleccione el tipo del lote</p>
								</div>
							</div>

							<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="numero_lote">
									<p class="help has-text-link">Escriba el numero del lote</p>
								</div>
							</div>
						</div>
					</div>

					<div class="field">
						<div class="columns">
							<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="medidas_lote">
									<p class="help has-text-link">Escriba las medidas del lote</p>
								</div>
							</div>

							<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="valor_lote">
									<p class="help has-text-link">Escriba el valor del lote</p>
								</div>
							</div>
						</div>
					</div>

					<div class="field">
						<div class="columns">
							<div class="column">
								<div class="control">
									<button class="button is-primary medium is-fullwidth">Crear Lote</button>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>

		<!-- ESTRUCTURA QUE MUESTRA LAS ALERTAS DE LOTES EN MORA -->
				<div class="column" style="display: block;">
						<div class="card">
							<div class="card-header">
								<p class="card-header-title">Listado Personas en Mora de mas de 5 Días</p>
							</div>
							<div class="card-content" style="max-height: 280px;overflow-y: scroll;">

								<div id="alerta_mora"></div>
							</div>
						</div>
				</div>
	</div>

	<div class="columns contenido_tablas_lotes">
		<div class="column is-6">
			<div class="card">
				<div class="card-header">
					<p class="card-header-title">Lotes de Vivienda</p>
				</div>
				<div class="card-content" style="max-height: 426px;overflow-y: scroll;">
					<div class="error-lotes-vivienda" style="display: none;">
						<p class="error-texto has-text-danger">No hay Lotes para mostar</p>
					</div>
					<table class="table is-fullwidth is-striped">
						<thead>
							<tr>
								<td>#</td>
								<td>Medidas</td>
								<td>Valor</td>
								<td>Estado</td>
								<td>Acciónes</td>
							</tr>
						</thead>
						<tbody id="cargar_lotes_vivienda">
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="column is-6">
			<div class="card">
				<div class="card-header">
					<p class="card-header-title">Lotes Comerciales</p>
				</div>
				<div class="card-content" style="max-height: 426px;overflow-y: scroll;">
					<div class="error-lotes-comerciales" style="display: none;">
						<p class="error-texto has-text-danger">No hay Lotes para mostar</p>
					</div>
					<table class="table is-fullwidth is-striped">
						<thead>
							<tr>
								<td>#</td>
								<td>Medidas</td>
								<td>Valor</td>
								<td>Estado</td>
								<td>Acciónes</td>
							</tr>
						</thead>
						<tbody id="cargar_lotes_comercial">
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="modal">
	<div class="modal-background"></div>
	<div class="modal-card">
		<form id="form_actualizar_lotes">
			<header class="modal-card-head">
				<p class="modal-card-title">Actualizar Datos del lote</p>
				<button type='button' class="cancel-update delete" aria-label="close"></button>
			</header>
			<section class="modal-card-body">
				<div class="content">

					<div class="field">
						<div class="columns">
							<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="medidas_update">
									<p class="help has-text-info">Medidas del lote</p>
								</div>
							</div>

							<div class="column is-half">
								<div class="control">
									<input type="text" class="input" name="valor_update">
									<p class="help has-text-info">Valor del lote</p>
								</div>
							</div>
						</div>
					</div>

					<div class="field">
						<input type="hidden" name="valor_tipo_lote">
						<input type="hidden" name="numero_lote">
					</div>

				</div>
				

			</section>
			<footer class="modal-card-foot">
				<button class="button is-success">Actualizar cambios</button>
				<a href="#" class="button is-danger cancel-update">Cancelar</a>
			</footer>
		</form>
	</div>
</div>


	<script type="text/javascript">
		var url_javascript = '<?= URL; ?>';
	</script>
	<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
	<script type="module" src="<?= URL . 'public/js/app_lotes.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>