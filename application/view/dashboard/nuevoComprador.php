<?php 
	if (empty($_SESSION['sesion_usuario'])) :
			$casa = URL;
			header("location: $casa");
		else:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | casas && lotes</title>
	<link rel="stylesheet" href="<?php echo URL ?>public/css/bulma-9.1.css">
	<style>
		body {background-color: rgb(247, 247, 247);}
	</style>
</head>
<body>
	<?php require_once( APP. 'view/_templates/nav-header.php'); ?>
	<div class="container mt-5">
		<div class="colums">
			<div class="column is-8">
				<h2 class="title is-4">Agregar un nuevo comprador</h2>
			</div>
		</div>

		<div class="columns">
			<div class="column is-10">
				<div class="card">
					<form id="form_nuevo_comprador" autocomplete="off">

						<div class="card-content">
							<div class="field">
								<label class="label">Datos del comprador</label>
								<div class="columns">
									<div class="column is-half">
										<div class="control">
											<input type="text" class="input" name="nombres">
											<p class="help has-text-link">Nombres del comprador</p>
										</div>
									</div>

									<div class="column is-half">
										<div class="control">
											<input type="text" class="input" name="apellidos">
											<p class="help has-text-link">Apellidos del comprador</p>
										</div>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="columns">
									<div class="column is-half">
										<div class="control">
											<div class="select is-fullwidth">
												<select name="tipo_documento" id="tipo_documento">
													<option value="0">Seleccione...</option>
													<option value="1">Cédula de ciudadanía</option>
													<option value="2">Cédula de extranjería</option>
													<option value="3">Pasaporte</option>
												</select>
											</div>
											<p class="help has-text-link">Selecciona el tipo de documento del comprador</p>
										</div>
									</div>

									<div class="column is-half">
										<input type="text" class="input" name="numero_identificacion">
										<p class="help has-text-link">Numero de identificación del comprador</p>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="columns">
									<div class="column is-half">
										<input type="text" class="input" name="direccion">
										<p class="help has-text-link">Dirección de residencia del comprador</p>
									</div>

									<div class="column is-half">
										<input type="text" class="input" name="correo_electronico">
										<p class="help has-text-link">Dirección correo electronico del comprador</p>
									</div>
								</div>
							</div>

							<div class="field">
								<div class="columns">
									<div class="column is-half">
										<input type="text" class="input" name="celular">
										<p class="help has-text-link">Numero celular del comprador</p>
									</div>

									<div class="column is-half">
										<div class="control">
											<div class="select is-fullwidth">
												<select name="dia_pago" id="dia_pago">
												</select>
											</div>
											<p class="help has-text-link">Selecciona el dia de pago para los abonos</p>
										</div>
									</div>
								</div>
							</div>
						</div><!--END CARD-CONTENT-->

						<div class="card-footer"style="display: flow-root;">
							<div class="columns">
								<div class="column">
									<button class="button is-medium is-primary my-4 is-fullwidth">Agregar Nuevo Comprador</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>


	</div>

	<script type="text/javascript">
		var url_javascript = '<?= URL; ?>';
	</script>
	<script src="<?= URL; ?>public/js/sweetalert.min.js"></script>
	<script type="module" src="<?= URL . 'public/js/app_comprador.js?version=' . microtime(); ?> "></script>
</body>
</html>

<?php endif; ?>