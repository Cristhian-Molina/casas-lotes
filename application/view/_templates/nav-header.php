	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
			<a class="navbar-item" href="#">
				<img src="<?php echo URL ?>public/img/logo.svg" width="112" height="28">
			</a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
		</div>

		<div id="navbarBasicExample" class="navbar-menu">
			<div class="navbar-start">
				<a class="navbar-item" href="<?php echo URL ?>dashboard/">Home</a>

				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">Compradores</a>

					<div class="navbar-dropdown">
						<a class="navbar-item" href="<?php echo URL ?>dashboard/comprador">Nuevo comprador</a>
						<a class="navbar-item" href="<?php echo URL ?>dashboard/actualizar_datos_comprador/">Actualizar datos comprador</a>
					</div>
				</div>
				<a class="navbar-item" href="<?php echo URL ?>dashboard/vendedores">Asesores</a>
				<a class="navbar-item" href="<?php echo URL ?>dashboard/lotes">Lotes</a>
				<a class="navbar-item" href="<?php echo URL ?>dashboard/desistimientos">Desistimientos</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">Estimado en Ventas</a>
					<div class="navbar-dropdown">
						<a class="navbar-item" href="<?php echo URL ?>dashboard/estimado_financiado">Estimado Financiado</a>
						<a class="navbar-item" href="<?php echo URL ?>dashboard/estimado_sin_interes">Estimado Sin Interes</a>
						<a class="navbar-item" href="<?php echo URL ?>dashboard/estimado_contado">Estimado Contado</a>
						<a class="navbar-item" href="<?php echo URL ?>dashboard/estimado_disponibles">Estimado No vendidos</a>
					</div>
				</div>
			</div>

			<div class="navbar-end">
				<div class="navbar-item">
					<div class="buttons">
						<a class="button is-primary" href="<?php echo URL ?>dashboard/cerrar_session/">
							<strong>Cerrar sesion</strong>
						</a>
					</div>
				</div>
			</div>
		</div>
	</nav>